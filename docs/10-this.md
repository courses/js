## This

he this keyword is a very important concept in JavaScript, and also a particularly confusing one to both new developers and thoseAll Functions are Methods

In JavaScript all functions are object methods.

If a function is not a method of a JavaScript object, it is a function of the global object (see previous chapter).  who have experience in other programming languages. In JavaScript, this is a reference to an object. The object that this refers to can vary, implicitly based on whether it is global, on an object, or in a constructor, and can also vary explicitly based on usage of the Function prototype methods bind, call, and apply



**Implicit Context** : There are four main contexts in which the value of this can be implicitly inferred:
- the global context
- as a method within an object
- as a constructor on a function or class
- as a DOM event handler
Generally, it is safer to use strict mode to reduce the probability of this having an unexpected scope. Rarely will someone want to refer to the window object using this.


**Explicit Context**
using call, apply, or bind, you can explicitly determine what this should refer to

https://www.digitalocean.com/community/conceptual_articles/understanding-this-bind-call-and-apply-in-javascript

https://dmitripavlutin.com/gentle-explanation-of-this-in-javascript/

https://www.youtube.com/watch?v=zE9iro4r918&feature=emb_rel_end

The JavaScript this keyword refers to the object it belongs to.
It has different values depending on where it is used:
- In a method, this refers to the owner object.
- Alone, this refers to the global object.
- In a function, this refers to the global object.
- In a function, in strict mode, this is undefined.
- In an event, this refers to the element that received the event.
- Methods like call(), and apply() can refer this to any object.


- Implicit binding. Dot Operator
- Explicit binding. Call (invoke), apply (), bind (retourne une fonction).
- new binding
- window binding, sinon 'use strict'





 Utilisé au sein d'une fonction, this fait référence à l'objet courant. Sa signification dépend de la façon dont la fonction a été appelée. Si elle a été appelée avec la notation utilisant le point ou les crochets sur un objet, cet objet devient this. Si cette notation n'a pas été utilisée pour l'appel, this fera référence à l'objet global. C'est une source fréquente d'erreurs. Par exemple :


function creerPersonne(prenom, nom) {
  return {
    prenom,
    nom,
    nomComplet: function() {
      return this.prenom + ' ' + this.nom;
    }
  }
}
var s = creerPersonne("Simon", "Willison");
var nomComplet = s.nomComplet;
nomComplet(); // undefined undefined



Global : this = window
Objet : this = instance
Classe : this = instance
Objet, inner fonction, this = instance
!!! Objet, inner fonction, callback (ex: forEach), this = window, eventlistener


Problème callback dans innerFn:
```javascript
const john = {
	name:'John',
	year: 1980,
	jobs: [ 'teacher', 'designer', 'woodworker' ],
	getJobs: function() {
    // Issue
		/*
    this.jobs.forEach(function(job) { // premier this ok.
			console.log(`${this.name} a été: ${job}`); // this dans CB pas ok.
		});
    */

    //solutions : arrow fn
    this.jobs.forEach(job => { // premier this ok.
      console.log(`${this.name} a été: ${job}`); // this dans CB pas ok.
    });
	}
}

john.getJobs();
```


Issue
Working around "The this inside the event listener callback will be the element that fired the event"
- https://stackoverflow.com/questions/30446622/es6-class-access-to-this-with-addeventlistener-applied-on-method
- https://stackoverflow.com/questions/43727516/javascript-how-adding-event-handler-inside-a-class-with-a-class-method-as-the-c/43727582

```javascript
document.querySelector(DOM.container).addEventListener('click', e => this.ctrlDeleteItem(e));
document.querySelector(DOM.inputType).addEventListener('change', () => this.UICtrl.changedType()); // if no parameter
```


### This and function references
First Head Javascript Book


Priority 1 . Hard Binding - bind
Priority 2 Explicit Binding Rule — call or apply
Priority 3 Implicit Binding rule
Default Case : Normal functions in global scope.

```javascript
const dj = {
    playsound: function() { // The playsound property contains an object reference: a pointer to a function object.
        console.log(`Playing ${this.sound}`);
    },
    sound: 'bells'
};

const controller = {
    start: function() {
        setInterval(dj.playsound, 1000); // dj.playsound is a reference to the fn

    }
};

dj.playsound(); // Playing bells
controller.start(); // Every 1 sec, Playing undefined

```

**The problem** : setInterval is calling the method like a function. And because we’re calling playsound as a function instead of as a method, this doesn’t get set to the dj object.

Usually, when we call a method of an object, we call it like this: dj.playsound(); When we call playsound as a method of the dj object, then this is correctly set to the dj object in the body of the playsound method, so everything works fine. But here, setInterval is getting passed the right method, but isn’t calling that method as a method; instead setInterval is calling it as a function, just as if you
tried to call playsound like this: playsound(); Without the “dj.” in front of the call to playsound , there’s no object to set this to.

Because setInterval is calling playsound as a function rather than as a method of the dj object, the value of this isn’t changed from the window object to another object (like it is when you call a method of an object).

To setInterval, playsound looks like just a regular function that’s disconnected from any particular object. There’s nothing in the function object that says “I belong to the dj object”. The fact that we use “dj.” in “dj.playsound” when we pass the function doesn’t mean the function object has any information about the dj object in it.

**Making sure this gets set correctly when the playsound method is called by setInterval**

Two solutions :
- setInterval(function() { dj.playsound(); }, 1000);
- setInterval(dj.playsound.bind(dj), 1000);
