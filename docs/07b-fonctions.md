
    **Paramètre par défaut**

    //ES5
    this.nationality = nationality || 'France';

    //ES6
    function eat(food, quantity = 3){}
    eat('Radis')





        ### Paramètres et arguments
        placeholder (paramètres fictif)


        Value (primitives) != référence (object)


        If arguments > number of arguments expected, the extra values will be ignored
        If arguments < number of arguments expected, the function will assume undefined in place of the missing arguments


        // Définition, signature de la fonction
        function eat(food, quantity){} // on spécifie des paramètres. label : blank
        // Appel de la fonction
        eat('Radis', 10) // on passe des arguments. assigne les paramètres


        Par définition les fonctions ont accès à des variables supplémentaires à l'intérieur de leur corps, appelée arguments. Ce sont des objets semblables à un tableau qui conservent toutes les valeurs reçues par la fonction.


        l'objet arguments, qui, non content d'être une liste des paramètres, fournit également une propriété appelée arguments.callee. Celle-ci se réfère toujours à la fonction courante et peut donc être utilisée pour des appels récursifs




        L'objet arguments n'est pas un Array. Il est similaire à un Array, mais il n'a pas les propriétés d'un Array, exceptée la propriété length. Par exemple, il n'a pas la méthode pop(). Néanmoins, il peut être converti en un vrai objet de type Array :
        var args = Array.from(arguments); ou var args = [...arguments];

        En fait, lorsqu'il n'y a aucun paramètre du reste, paramètre par défaut ou aucune décomposition, les arguments formels feront références aux valeurs de l'objet arguments.



        **Paramètres du reste (Rest parameters)**
        Cette syntaxe permet de représenter un nombre indéfini d'arguments sous forme d'un tableau.

        function declaration: list to array


        Il y a trois principales différences entre les paramètres du reste et l'objet arguments :

            les paramètres du reste sont uniquement ceux qui ne possèdent pas de noms à part entière (autrement dit ceux qui ne sont pas formellement définis dans l'expression de fonction), l'objet arguments contient chaque argument passé à la fonction
            l'objet arguments n'est pas, à strictement parler, un tableau. Le paramètre représentant les arguments restant est une instance d'Array à laquelle on peut appliquer directement des méthodes comme sort, map, forEach ou pop
            l'objet arguments possède des fonctionnalités spécifiques (comme, par exemple, la propriété callee)


    the rest parameter, you can create functions that take a variable number of arguments. These arguments are stored in an array that can be accessed later from inside the function. The rest parameter eliminates the need to check the args array and allows us to apply map(), filter() and reduce() on the parameters array.


        Avec la décomposition des arguments (ES2015/ES6) (cf. les paramètres du reste) et let, on pourrait écrire une version équivalente :

        function moyenne(...args) {
          var somme = 0;
          for (let valeur of args) {
            somme += valeur;
          }
          return somme / args.length;
        }
        moyenne(2, 3, 4, 5); // 3.5

        // Astuce avec les tableaux
        moyenne.apply(null, [2, 3, 4, 5]); // 3.5

        On peut également utiliser l'opérateur de décomposition pour l'appel et la définition de la fonction pour écrire, par exemple, moyenne(...nombres).






        ### Méthodes
        You can use call()/apply() to invoke the function immediately. bind() returns a bound function that, when executed later, will have the correct context ("this") for calling the original function. So bind() can be used when the function needs to be called later in certain events when it's useful.

        call() or Function.prototype.call()
        apply() or Function.prototype.apply()
        bind() or Function.prototype.bind()

        **call et apply**
        The apply() method is similar to the call() method. The difference :
        - The call() method takes arguments separately.
        - The apply() method takes arguments as an array.


        call: appel d'une fonction avec comme premier argument la valeur de this.
        permet de définir la valeur de this mais qui prend une liste d'arguments plutôt qu'un tableau.

        The call() method is a predefined JavaScript method.
        It can be used to invoke (call) a method with an owner object as an argument (parameter).
        With call(), an object can use a method belonging to another object.

        ```javascript
        var person = {
          fullName: function(city, country) {
            return this.firstName + " " + this.lastName + "," + city + "," + country;
          }
        }
        var person1 = {
          firstName:"John",
          lastName: "Doe"
        }
        person.fullName.call(person1, "Oslo", "Norway");
        person.fullName.apply(person1, ["Oslo", "Norway"]);
        ```

        **apply**
        Method Reuse. With the apply() method, you can write a method that can be used on different objects.

        In JavaScript strict mode, if the first argument of the apply() method is not an object, it becomes the owner (object) of the invoked function. In "non-strict" mode, it becomes the global object

        ```javascript
        objA.sayName().apply(objB); // la méthode appliquée à l'objB. Changed the context ( pour this)

        ```


        **bind**
        Both call and apply are one-time use methods—if you call the method with the this context it will have it, but the original function will remain unchanged. Sometimes, you might need to use a method over and over with the this context of another object, and in that case you could use the bind method to **create a brand new function with an explicitly bound this**


        La méthode bind() crée une nouvelle fonction qui, lorsqu'elle est appelée, a pour contexte this la valeur passée en paramètre et éventuellement une suite d'arguments qui précéderont ceux fournis à l'appel de la fonction créée.


        ```javascript
        const module = {
          x: 42,
          getX: function() {
            return this.x;
          }
        }
        const unboundGetX = module.getX;
        // The function gets invoked at the global scope
        console.log(unboundGetX());  // expected output: undefined
        const boundGetX = unboundGetX.bind(module);
        console.log(boundGetX()); // expected output: 42
        ```

        Fonctions partiellement appliquées. The next simplest use of bind() is to make a function with pre-specified initial arguments.

        on utilise bind() afin de créer une fonction avec des arguments initiaux prédéfinis. Ces arguments, s'il y en a, suivent le this fourni et sont ensuite insérés au début des arguments passés à la fonction cible, suivis par les arguments passés à la fonction liée au moment où celle-ci est appelée.

        ```javascript
        function bonjour() { console.log('Arguments:', arguments)}
        const bonjourJerome = bonjour.bind(null, 'Jérôme');
        bonjourJerome(); // ['Jérôme']
        bonjourJerome('Abel', 42, 'ans'); // ['Jérôme','Abel', 42, 'ans']
        ```
