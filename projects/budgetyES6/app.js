// **************** BUDGET **************** //

// Budget classes
class BudgetItem {
    constructor(id, description, value){
        this._id = id;
        this._description = description;
        this._value = value;
    }
    get id() { return this._id; }
    get description() { return this._description; }
    get value() { return this._value; }
}

// Expense
class Expense extends BudgetItem {
    constructor(id, description, value){
        super(id, description, value);
        this._percentage = -1;
    } 
    get percentage() { return this._percentage; }  
    set percentage(val) { this._percentage = val; }
    
    calcPercentage(totalIncome) {
        let val = -1;
        if (totalIncome > 0) val = Math.round((this._value / totalIncome) * 100);
        this.percentage = val; 
    }
}

// Income
class Income extends BudgetItem {
    constructor(id, description, value){
        super(id, description, value);
    }
}

// Budget Controller
class BudgetController {
    
    constructor(){ // data model
        this._items = { exp: [], inc: [] };
        this._totals = { exp: 0, inc: 0 };
        this._budget = 0;
        this._percentage = -1;
    }
    
    get items() { return this._items; }
    get totals() { return this._totals; }
    get budget() { return this._budget; }
    get percentage() { return this._percentage; }
    
    set budget(val) { this._budget = val; }
    set percentage(val) { this._percentage = val; }
    
    addItem(type, des, val) {
        let newItem, ID;
        
        // New ID
        if (this.items[type].length > 0) {
            ID = this.items[type][this.items[type].length - 1].id + 1; // ?? or ._id
        } else {
            ID = 0;
        }
        
        // New item
        if (type === 'exp') newItem = new Expense(ID, des, val);
        else if (type === 'inc') newItem = new Income(ID, des, val);
        
        // Record data
        this.items[type].push(newItem);
        
        // Return the new element
        return newItem;
    }
    
    deleteItem(type, id) {
        let index = this.items[type].findIndex(el => el.id === id); // ?? or el._id
        if (index !== -1) this.items[type].splice(index, 1);
    }

    calculateTotal(type) {
        let sum = this.items[type].reduce( (acc, cur) => acc + cur.value, 0 );
        this.totals[type] = sum;
    }

    calculateBudget() {
        // Calculate the budget
        this.calculateTotal('exp');
        this.calculateTotal('inc');
        this.budget = this.totals.inc - this.totals.exp;
        // Calculate the percentage
        if (this.totals.inc > 0) this.percentage = Math.round((this.totals.exp / this.totals.inc) * 100);
        else this.percentage = -1;           
    }
    
    calculatePercentages() {
        this.items.exp.forEach(cur => cur.calcPercentage(this.totals.inc));
    }    
    
    getPercentages() {
        return this.items.exp.map(cur => cur.percentage);
    }
        
    getBudget() {
        return {
            budget: this.budget,
            totalInc: this.totals.inc,
            totalExp: this.totals.exp,
            percentage: this.percentage
        }
    }

    debug(){ 
        console.log('Items: ', this.items); // ?? template strings avec Object or Arrays
        console.log('Totals: ', this.totals);
        console.log('Budget: ', this.budget);
        console.log('Percentage: ', this.percentage); 
    }
}


// **************** UI  **************** //
class UIController {
    constructor(){
        this._DOMstrings = {
            inputType: '.add__type',
            inputDescription: '.add__description',
            inputValue: '.add__value',
            inputBtn: '.add__btn',
            incomeContainer: '.income__list',
            expensesContainer: '.expenses__list',
            budgetLabel: '.budget__value',
            incomeLabel: '.budget__income--value',
            expensesLabel: '.budget__expenses--value',
            percentageLabel: '.budget__expenses--percentage',
            container: '.container',
            expensesPercLabel: '.item__percentage',
            dateLabel: '.budget__title--month'
        };
    }
    
    get DOMstrings() { return this._DOMstrings; }
    
    formatNumber(num, type) { // ?? private ??
        let numSplit, int, dec;
        num = Math.abs(num);
        num = num.toFixed(2);
        numSplit = num.split('.');
        int = numSplit[0];
        if (int.length > 3) {
            int = int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, 3); //input 23510, output 23,510
        }
        dec = numSplit[1];
        return (type === 'exp' ? '-' : '+') + ' ' + int + '.' + dec;
    }
    
    getInput() {
        return {
            type: document.querySelector(this.DOMstrings.inputType).value, // Will be either inc or exp
            description: document.querySelector(this.DOMstrings.inputDescription).value,
            value: parseFloat(document.querySelector(this.DOMstrings.inputValue).value)// convert strings to float
        }
    }
    
    addListItem(obj, type) {
        let html, newHtml, element;
        // Create HTML string with placeholder text
        if (type === 'inc') {
            element = this.DOMstrings.incomeContainer;
            html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
        } else if (type === 'exp') {
            element = this.DOMstrings.expensesContainer;
            html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
        }
            
        // Replace the placeholder text with some actual data
        newHtml = html.replace('%id%', obj.id);
        newHtml = newHtml.replace('%description%', obj.description);
        newHtml = newHtml.replace('%value%', this.formatNumber(obj.value, type));
            
        // Insert the HTML into the DOM
        document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
    }
    
    deleteListItem (selectorID) {
        const el = document.getElementById(selectorID);
        el.parentNode.removeChild(el);
    }
    
    clearFields() {
        let fields, fieldsArr;
        fields = document.querySelectorAll(this.DOMstrings.inputDescription + ', ' + this.DOMstrings.inputValue);
        fieldsArr = Array.from(fields);
        fieldsArr.forEach( cur => cur.value = "" ); // ?? Inline arrow function without returning a value return avec {}
        fieldsArr[0].focus();
    }
    
    displayBudget(obj) {
        let type;
        obj.budget > 0 ? type = 'inc' : type = 'exp';           
        document.querySelector(this.DOMstrings.budgetLabel).textContent = this.formatNumber(obj.budget, type);
        document.querySelector(this.DOMstrings.incomeLabel).textContent = this.formatNumber(obj.totalInc, 'inc');
        document.querySelector(this.DOMstrings.expensesLabel).textContent = this.formatNumber(obj.totalExp, 'exp'); 
        if (obj.percentage > 0) {
            document.querySelector(this.DOMstrings.percentageLabel).textContent = obj.percentage + '%';
        } else {
            document.querySelector(this.DOMstrings.percentageLabel).textContent = '---';
        }
    }
    
    displayPercentages(percentages) {    
        const fields = document.querySelectorAll(this.DOMstrings.expensesPercLabel);
        const fieldsArr = Array.from(fields);
        fieldsArr.forEach( (cur, index) => {
            if (percentages[index] > 0)  {
                cur.textContent = percentages[index] + '%';
            } else {
                cur.textContent = '---';
            }
        });
    }
    
    displayMonth() {
        let now, month, year;
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        now = new Date();            
        month = now.getMonth();
        year = now.getFullYear();
        document.querySelector(this.DOMstrings.dateLabel).textContent = months[month] + ' ' + year;
    }
    
    changedType() {
        const fields = document.querySelectorAll(
            this.DOMstrings.inputType + ',' +
            this.DOMstrings.inputDescription + ',' +
            this.DOMstrings.inputValue
        );
        const fieldsArr = Array.from(fields);
        fieldsArr.forEach( cur => {
               cur.classList.toggle('red-focus'); 
        });
        document.querySelector(this.DOMstrings.inputBtn).classList.toggle('red');
    }
}


// **************** APP **************** //
class App {
    constructor(budgetCtrl, UICtrl) {
        this._budgetCtrl = budgetCtrl;
        this._UICtrl = UICtrl;
    }
    get budgetCtrl() { return this._budgetCtrl; }
    get UICtrl() { return this._UICtrl; }
    
    init(){
        console.log('Application has started.');
        this.UICtrl.displayMonth();
        this.UICtrl.displayBudget({
            budget: 0,
            totalInc: 0,
            totalExp: 0,
            percentage: -1
        });
        this.setupEventListeners();
    }
    
    setupEventListeners() {
        const DOM = this.UICtrl.DOMstrings;
        document.querySelector(DOM.inputBtn).addEventListener('click', this.ctrlAddItem);
        document.addEventListener('keypress', event => {
            if (event.keyCode === 13 || event.which === 13) {
                this.ctrlAddItem();
            }
        });
        
        /*
        ?? Issue
        Working around "The this inside the event listener callback will be the element that fired the event"
        - https://stackoverflow.com/questions/30446622/es6-class-access-to-this-with-addeventlistener-applied-on-method
        - https://stackoverflow.com/questions/43727516/javascript-how-adding-event-handler-inside-a-class-with-a-class-method-as-the-c/43727582
        */
        
        document.querySelector(DOM.container).addEventListener('click', e => this.ctrlDeleteItem(e)); 
        document.querySelector(DOM.inputType).addEventListener('change', () => this.UICtrl.changedType()); // if no parameter
    }
    
    updateBudget() {
        this.budgetCtrl.calculateBudget();
        const budget = this.budgetCtrl.getBudget();
        this.UICtrl.displayBudget(budget);
    }
    
    updatePercentages() {
        this.budgetCtrl.calculatePercentages();
        const percentages = this.budgetCtrl.getPercentages();
        this.UICtrl.displayPercentages(percentages);
    }
    
    ctrlAddItem() {
        let input, newItem;
        
        input = this.UICtrl.getInput();        
        
        if (input.description !== "" && !isNaN(input.value) && input.value > 0) {
            newItem = this.budgetCtrl.addItem(input.type, input.description, input.value);
            this.UICtrl.addListItem(newItem, input.type);
            this.UICtrl.clearFields();
            this.updateBudget();
            this.updatePercentages();
        }
    }
    
    ctrlDeleteItem(event) {
        let itemID, splitID, type, ID;
        
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
                
        if (itemID) {
            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);

            this.budgetCtrl.deleteItem(type, ID);
            this.UICtrl.deleteListItem(itemID);
            this.updateBudget();
            this.updatePercentages();
        }
    }
}

// **************** TEST SEQUENCE **************** //
/*
const b = new BudgetController();
const ui = new UIController();

// Populate items
ui.addListItem(b.addItem('inc', 'P1', 1500), 'inc');
ui.addListItem(b.addItem('inc', 'P2', 500), 'inc');
ui.addListItem(b.addItem('exp', 'Rent', 1000), 'exp');
ui.addListItem(b.addItem('exp', 'Rent2', 500), 'exp');
// b.deleteItem('inc', 0);
b.calculateBudget();
b.calculatePercentages();

// Display
b.debug();
ui.displayBudget(b.getBudget());
ui.displayPercentages(b.getPercentages());
ui.displayMonth();
*/

// **************** START APP **************** //
const app = new App(new BudgetController(), new UIController());
app.init();