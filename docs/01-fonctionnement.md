# FONCTIONNEMENT
Les moteurs JS interprètent/compilent à la volée les scripts JS, puis les exécutent. Dans le sens où il y a au moins une étape de compilation du code en un langage machine puis une étape d'exécution, JS est considéré comme un langage compilé.

## Les moteurs JS (JS Engine)
Les [moteurs JS](https://en.wikipedia.org/wiki/List_of_ECMAScript_engines) sont typiquement développés par les fournisseurs de navigateurs Web. Dans le navigateur, le moteur JS fonctionne de concert avec le moteur de rendu via le DOM (Document Object Model). La plupart sont implémentés en C et C++,  Rhino lui est écrit en Java.

| Navigateurs        								| Moteurs JS           										| Moteurs de rendu  |
| --------------------------------- | --------------------------------------- | ----------------- |
| Firefox      											| SpiderMonkey, TraceMonkey, JägerMonkey	| Gecko							|
| Google Chrome, Chromium, Opéra		| V8																			| Blink							|
| Safari      											| JavaScriptCore													| WebKit						|
| Microsoft Edge, Internet Explorer | Chakra / ChakraCore											| Trident						|


## Compilation à la volée (compile time)

### Le meilleur des deux mondes

In programming, there are generally two ways of translating to machine language. You can use an **interpreter** or a **compiler** [ahead-of-time compilation (AOT)]. The con of using an interpreter comes when you’re running the same code more than once. For example, if you’re in a loop. Then you have to do the same translation over and over and over again. The compiler has the opposite trade-offs. It takes a little bit more time to start up because it has to go through that compilation step at the beginning. But then code in loops runs faster, because it doesn’t need to repeat the translation for each pass through that loop. [With Just-In-Time compilers], different browsers do this in slightly different ways, but the basic idea is the same. They added a new part to the JavaScript engine, called a monitor (aka a profiler). That monitor watches the code as it runs, and makes a note of how many times it is run and what types are used. [1]

![InterpreterdCompiled](img/06aa-interpreted-compiled.png)

The dynamic type system that JavaScript uses requires a little bit of extra work at runtime.[...] Its compilation is not made in the traditional way (compiling all the code before) but happens every time before the execution of the code:
- **Engine**: the boss, program included in the browser, this dude is responsible for starting and finishing the compilation and executes the code.
- **Compiler**: friend of Engine, this guy does the compiling itself, the way it’s explained in the previous section.
- **Scope**: friend of Engine and Compiler, scope is responsible for knowing all the declared variables and which is their accessibility.

Exemple: `var a = 4;`. Engine sees this statement in two pieces:
- **Compiling part** : Engine > Compiler : break chunks / tree + code generation. Compiler > Scope : creates var a if not exist.
- **Execution Part** : Compiler > Scope : accès à la variable. Compiler > Engine : assigner la valeur 4 if exists, sinon throw Error :
  - `ReferenceError` (variable not found in any of the scopes)
  - `TypeError` (variable was found, but that you tried to do something with it that is impossible, like trying to execute-as-function a non-function value, reference a property on a null or undefined value.) [2]


### Étapes de la compilation

![Compilation](img/06a-compilation.jpg)

1. The HTML parser encounters a script tag with a source.
2. The script gets loaded as a UTF-16 byte stream from either the network, cache or service worker, and passed to a byte stream decoder (0066 ...).
3. Lexical analysis : The byte stream decoder (lexer / tokenizer / scanner ?) decodes the bytes into tokens (0066 decodes to f), sent to the parser/preparser (keyword, identifier, ponctuator, variable type, ...). The preparser handles code that may be used later on (like click event, ...)
4. Syntactic analysis : The parser creates nodes based on the tokens and creates an Abstract Syntax Tree (AST). Collects scopes.
5. The interpreter (V8/Ignition) walks through the AST and generates byte code. Once the byte code has been generated fully, the AST is deleted, clearing up memory space.
6. The byte code with the generated type feedback is sent to an optimizing compiler (V8/TurboFan), which generates highly optimized machine code (+deoptomization (or bailing out)). In order to make a faster version of the code, the optimizing compiler has to make some assumptions.

Usually optimizing compilers make code faster, but sometimes they can cause unexpected performance problems. If you have code that keeps getting optimized and then deoptimized, it ends up being slower than just executing the baseline compiled version. [3]


## Gestion de la mémoire

When a software runs on a target Operating system on a computer it needs access to the computers RAM (Random-access memory) to:
- load its own bytecode that needs to be executed
- store the data values and data structures used by the program that is executed
- load any run-time systems that are required for the program to execute
When a software program uses memory there are two regions of memory they use, apart from the space used to load the bytecode, **Stack** and **Heap** memory. [4]

| **Static allocation**               | **Dynamic allocation**     	        |
| -----------------------             | -----------------------             |
| Size must be known at compile time  | Size may be unknown at compile time |
| Perform at **compile time**         | Perform at **run time**             |
| Asssigned to a **stack**            | Assigned to the **heap**            |
| FILO (First-in Last-out)            | No particular order of assignment   |

When you compile your code, the compiler can examine primitive data types and calculate ahead of time how much memory they will need. The required amount is then allocated to the program in the **call stack** space. The space in which these variables are allocated is called the stack space because as functions get called, their memory gets added on top of the existing memory. As they terminate, they are removed in a LIFO (last-in, first-out) order. Unfortunately, things aren’t quite as easy when we don’t know at compile time how much memory a variable will need. Instead, our program needs to explicitly ask the operating system for the right amount of space at run-time. This memory is assigned from the **heap** space. [5]

Low-level languages like C, have manual memory management primitives such as `malloc()` and `free()`. In contrast, JavaScript **automatically allocates memory** when objects are created and frees it when they are not used anymore (garbage collection). Regardless of the programming language, the memory life cycle is pretty much always the same:
- Allocate the memory you need
- Use the allocated memory (read, write)
- Release the allocated memory when it is not needed anymore [6]

The second part is explicit in all languages. The first and last parts are explicit in low-level languages but are mostly implicit in high-level languages like JavaScript. [They] utilize a form of automatic memory management known as **garbage collection** (GC). The purpose of a garbage collector is to monitor memory allocation and determine when a block of allocated memory is no longer needed and reclaim it. This automatic process is an **approximation** since the general problem of determining whether or not a specific piece of memory is still needed is undecidable. [...] The hardest task here is to figure out when the allocated memory is not needed any longer.

In a non-dynamic language like Java, a property’s location in memory can often be determined with only a single instruction whereas in Javascript several instructions are required to retrieve the location from a hash table. As a result, **property lookup is much slower in Javascript** than it is in other languages. [7]

![Memory Management](img/07b-memory-managment.gif)

Voir le fonctionnement en [images](https://speakerdeck.com/deepu105/v8-memory-usage-stack-and-heap). Pour le moteur V8, les primitives (types simples) semblent être mémorisées dans la pile (stack), tandis que les objets et fonctions dans le Heap, avec une référence dans la pile.

The Stack [...] is automatically managed and is done so by the operating system rather than V8 itself. Hence we do not have to worry much about the Stack. The Heap, on the other hand, is not automatically managed by the OS and since its the biggest memory space and holds dynamic data, **it could grow** exponentially causing our program to run out of memory over time. It also becomes **fragmented** over time slowing down applications. This is where garbage collection comes in. In simple terms, it frees the memory used by orphan objects, i.e, objects that are no longer referenced from the Stack directly or indirectly (via a reference in another object) to make space for new object creation. [8]

The heap, which is also called "free store", is a large region in memory, which can be used to store arbitrary data in an unordered fashion. That's why it's a lot slower, and used for data structures in classic languages, like C. A lot of science went into optimizing the way space is allocated in heap, so that there is a minimum number of gaps in between data. In order to quickly find heap-data when doing operations, a pointer to it is stored on the stack. [hasnode](https://hashnode.com/post/does-javascript-use-stack-or-heap-for-memory-allocation-or-both-cj5jl90xl01nh1twuv8ug0bjk)


## Exécution (runtime)

![Execution](img/08a-event-loop.jpg)

Le moteur JS fonctionne dans le navigateur qui lui fournit les services des Web API pour manipuler les documents HTML (DOM), les événements du clavier, de la souris (Events), les requêtes asynchrones (AJAX), etc. La gestion de ces échanges se fait grâce à une [boucle d'événements](https://developer.mozilla.org/fr/docs/Web/JavaScript/Concurrence_et_boucle_des_%C3%A9v%C3%A9nements) (event loop) et d'une file de messages à traiter (message queue / callback queue). Les APIs, l'event loop et la file de messages sont fournis par le navigateur. Les moteurs JS ne s'occupent que de ce qui se passe dans les mémoires de la pile (stack) et Heap.

Tous les appels de fonctions sur la pile d'exécution (execution stack / call stack) sont dit synchrones, ils s'exécutent directement. Dès qu'une fonction des API Webs est appelée, elles sont mises en attentes et gérées par le navigateur, leur retours sont dit asynchrones.


### Pile d'exécution (stack)

JavaScript, a **single-threaded** programming language, which means it has a single Call Stack and thus it has the ability to do **one thing at a time**. The Call Stack is basically a data structure which records the point in the program which is actually executing. This call stack is similar to other stack having the basic functionality like, if we step into a function, we put it on the top of the stack. We have to pop off the top of the stack if we want to return from a function. This is the basic functionality of every stack. [9]

![Call stack](img/09a-call-stack.gif)

Cinq points à se souvenir sur la pile d'exécution : [10]
- Un seul fil d'exécution (single-threaded) ;
- Exécution synchrone, un seul contexte actif sur la pile ;
- Un contexte global ;
- Un nombre infini de contextes de fonctions ;
- Chaque appel de fonction crée un nouveau contexte d'exécution, même pour un appel récursif.


### Boucle d'événements (event loop)

What happens when you have function calls in the Call Stack that take a huge amount of time in order to be processed? For example, imagine that you want to do some complex image transformation with JavaScript in the browser. The problem is that while the Call Stack has functions to execute, the browser can’t actually do anything else — it’s getting blocked. This means that the browser can’t render, it can’t run any other code, it’s just stuck. And this creates problems if you want nice fluid UIs in your app. [...] And most browsers take action by raising an error, asking you whether you want to terminate the web page. So, how can we execute heavy code without blocking the UI and making the browser unresponsive? Well, the solution is **asynchronous callbacks**. [9]

Pour éviter de bloquer le navigateur, une technique est de placer des fonctions dans un `setTimeout ( ..., 0)` avec un delay de 0. Le message ne sera pas directement executé mais passera par la gestion de la boucle d'événements et de la file de message.

Gestion de la concurrence :
1. Quand une fonction est appelée, elle s'ajoute (pushed to) sur la pile d'exécution (call stack) et se retire (popped off) quand elle retourne une valeur ;
2. Quand une fonction fournie par le navigateur est appelée, comme `setTimeout`, l'API Web gère la fonction de rappel (callback) ;
3. Quand le temps est écoulé (par exemple 1000ms), la fonction de rappel est envoyée à la file de messages à traiter (callback queue) ;
4. La boucle d'événements regarde la file de messages et la pile d'exécution. Si la pile est vide, elle pousse le premier élément de la file sur la pile.
5. La fonction de rappel est exécutée et retirée quand elle retourne une valeur. [11]

Exemple :
```javascript
const foo = () => console.log("First");
const bar = () => setTimeout(() => console.log("Second"), 500);
const baz = () => console.log("Third");

bar();
foo();
baz();
// Retourne : First, Third et après 500ms Second
```

![Event Loop](img/8c-event-loop.gif)


### Contexte d'exécution (execution context)

Les contextes d'exécutions permettent au moteur JS de gérer la complexité d'interprétation et d'exécution du code. Cela sert à suivre l'évaluation du code. À chaque instant, il n'y a qu'un seul contexte d'exécution qui exécute du code. Il est toujours au dessus de la pile et s'appelle le "running execution context". [12] [13]

Leur fonctionnement permet de comprendre les portées des variables, les comportements des fermetures (closures), hoisting, certains messages d'erreurs, etc.

Il y a deux types de contexte d'exécution :
1. Un unique **contexte d'exécution global** (Global execution context, GEC) est créé avant toute exécution de code, à la lecture de la balise `<script></script>`;
2. Puis, quand une fonction est appelée (executed/called/invoked), un nouveau **contexte d'exécution** (execution context, EC ou function execution context) est créé qui se place donc au dessus de la pile. D'où un phénomène de saturation (stack overflow) si trop de CE sont créés.

Chaque contexte s'exécute en deux temps : la **phase de création** qui mémorise des informations sur l'environnement, puis la **phase d'exécution** ou d'activation qui exécute le code ligne par ligne. Les détails de ces mécanismes peuvent diverger selon les navigateurs. Il y a la spécification d'[ECMAScript 2015](http://www.ecma-international.org/ecma-262/6.0/) qui n'est que théorique et l'implémentation dans les navigateurs. De plus, certains termes sur le Web font référence à l'ancienne version d'ECMAScript, et les interprétations visuelles peuvent être différentes, ce qui ajoute un peu à la difficulté de bien saisir tous ces aspects.

**Équivalences** :
- Lexical Environment ~= Variable Environment
- Environment Records = Variable Object (VO pour global) ou Activation Object (AO pour fonction), ancienne version d'ECMAScript
- Outer/Parent reference environment = Scope chain


#### 1. Phase de création
Création d'un environnement lexical (Lexical Environment), qui contient un objet Environment Records et une référence vers l'environnement lexical parent (outer scope), et résolution de la valeur `this`.

1.1. **This**
Détermine la valeur de `this` (this binding) quand la fonction est appelée :
- pour les appels de fonctions régulières (regular function) : le mot-clé "this" pointe vers l'objet global (window pour les navigateurs, global pour node), même si la fonction est dans une autre fonction (inner function) ou dans une méthode d'objet ;
- pour les appels de méthodes (propriétés d'un objet) : "this" pointe vers l'objet qui l'appelle.

1.2. **Portée parente** (outer scope)
Une référence vers l'environnement parent est créé, ce qui permet d'avoir accès aux variables des autres contextes. La portée est dit **lexicale**, car l'ordre est celui dans lequel les fonctions ont été écrites dans le code (lexical scoping), non l'ordre d'exécution :
- chaque fonction crée une portée ;
- chaque fonction a accès à la portée de l'objet globale et de la fonction parente (inner function < outer fonction).

1.3. **Environment Records**
Création des propriétés de l'objet Environment Records :
- création de l'objet `window` pour le contexte global (GEC) ou `arguments` pour les fonctions (EC) ;
- création des propriétés et assignation (binding) des valeurs par défaut (**hoisting** : accessibilité avant l'exécution) :
  - les fonctions déclarées sont prioritaires, leurs codes/définitions ("lambda") sont copiés en mémoire (heap) sans être lues (sans interprétation donc) et une référence (binding) est créée pour les rendre accessibles ;
  - les arguments et les déclarations de variables sont créés et assignés avec une valeur par défaut : `undefined` pour var et `uninitialized` pour let et const.

Cette étape permet de préparer et d'allouer la mémoire nécessaire (heap), pour y placer les variables accessibles en local (portée locale). La résolution des identifiants de ces variables (identifier resolution) se fait en recherche par la gauche (LHS, left hand side lookup) et suivant les portées locales et parentes, on parle de chaîne de portée (scope chain lookup). JS commence par la portée la plus locale et cherche ensuite en remontant cette chaîne. S'il ne la trouve pas, il renvoie une `ReferenceError`. De mettre pour let et const, si les variables sont seulement déclarées.

#### 2. Phase d'exécution
(ou Activation phase / Code Execution phase)

Le code de la fonction qui a généré le contexte courant est lu ligne par ligne :
  - Les variables sont assignées en recherche par la droite (RHS, right hand side)
  - Le code est ensuite interprété / exécuté.
  - On peut appelé une fonction avant sa déclaration, puisqu'on a sa référence (hoisting)


## Exemples

<img src="img/10a-execution-context-object.png" width="600px" alt="execution-context-object" />

### Exemple 1
```javascript
lastName; // ReferenceError
name; // undefined (hoisting)
calcAge; // undefined (hoisting)
var name = 'Bruce'; // obsolete var keywords are bound to the global object
name === window.name; // true
yearOfBirth = 1940; // undeclared variables are assigned and bound to the global object
yearOfBirth === window.yearOfBirth; // true
let age; // uninitialized (~undefined in the console)
function calcAge(year) { // function declaration copied in memory
  return year - yearOfBirth;
}
age = calcAge(2020); // 80
```

![Execution Context Example 1](img/10b-execution-context-example1.png)


### Autres exemples
- [Arindam Paul - JavaScript VM internals, EventLoop, Async and ScopeChains](https://www.youtube.com/watch?v=QyUFheng6J0)
- [JavaScript Internals](https://www.slideshare.net/nirnoy9/javascript-internals)
- [JavaScript Visualized Hoisting](https://dev.to/lydiahallie/javascript-visualized-hoisting-478h)
- [Understanding Execution Context and Execution Stack in Javascript](https://blog.bitsrc.io/understanding-execution-context-and-execution-stack-in-javascript-1c9ea8642dd0)
- [Ultimate guide to execution contexts, hoisting, scopes and closures](https://tylermcginnis.com/ultimate-guide-to-execution-contexts-hoisting-scopes-and-closures-in-javascript/)
- [What is the execution context in JavaScript ?](http://davidshariff.com/blog/what-is-the-execution-context-in-javascript/)

----

- [1] [A crash course in JIT compilers](https://hacks.mozilla.org/2017/02/a-crash-course-in-just-in-time-jit-compilers/)
- [2] [Compilation and Scope in Javascript](https://medium.com/@margalida.kaskante/compilation-and-scope-in-javascript-ebab7f4a6d64)
- [3] [JavaScript Visualized: the JavaScript Engine](https://dev.to/lydiahallie/javascript-visualized-the-javascript-engine-4cdf)
- [4] [Memory Management in programming](https://deepu.tech/memory-management-in-programming/)
- [5] [How JavaScript works: memory management](https://blog.sessionstack.com/how-javascript-works-memory-management-how-to-handle-4-common-memory-leaks-3f28b94cfbec)
- [6] [MDN Memory Management](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Memory_Management)
- [7] [Hidden Classes](https://richardartoul.github.io/jekyll/update/2015/04/26/hidden-classes.html)
- [8] [Memory Management in V8](https://deepu.tech/memory-management-in-v8/)
- [9] [How JavaScript Works](https://www.educba.com/how-javascript-works/)
- [10] [What is the execution context in JavaScript](http://davidshariff.com/blog/what-is-the-execution-context-in-javascript/)
- [11] [Lydia Hallie](https://dev.to/lydiahallie/javascript-visualized-event-loop-3dif)
- [12] [The Ultimate Guide to Hoisting, Scopes, and Closures in JavaScript](https://tylermcginnis.com/ultimate-guide-to-execution-contexts-hoisting-scopes-and-closures-in-javascript/)
- [13] [JS Demystified 04 — Execution Context](https://codeburst.io/js-demystified-04-execution-context-97dea52c8ac6)
- [14] [The Complete JavaScript Course : Build Real Projects ](https://www.udemy.com/course/the-complete-javascript-course)

Images :
- [Essentials of interpretation](http://dmitrysoshnikov.com/courses/essentials-of-interpretation/)
- [JavaScript engine fundamentals: Shapes and Inline Caches](https://mathiasbynens.be/notes/shapes-ics)
- [11]
- [Js Engine Pipeline](img/06b-js-engine-pipeline.png)
- [Execution Context Object](img/10a-execution-context-object.png)

Autres ressources :
- [Chrome University 2018: Life of a Script](https://youtu.be/voDhHPNMEzg) (vidéo)
- [How the Javascript engine works | The basics](https://www.youtube.com/watch?v=KM9coMpy5sQ) (vidéo)
- "Une optimisation est un compromis", [Quelques rouages d'un moteur JS](https://zestedesavoir.com/articles/1652/quelques-rouages-dun-moteur-javascript/)
- [How JS works un Browser and Node](https://cybrohosting.com/knowledgebase/17/How-JavaScript-works-in-browser-and-node.html)
- [How Browsers Works](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/)
- [JavaScript visualizer](https://tylermcginnis.com/javascript-visualizer/)
- [JavaScript Tutorial Part 1 - Lexical Environment](https://www.youtube.com/watch?v=_p1dwxpxGIw)
- [Execution Context, Lexical Environment, and Closures in JavaScript](https://medium.com/better-programming/execution-context-lexical-environment-and-closures-in-javascript-b57c979341a5)
- [How JavaScript works: an overview of the engine, the runtime, and the call stack](https://blog.sessionstack.com/how-does-javascript-actually-work-part-1-b0bacc073cf)
