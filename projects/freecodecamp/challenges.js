function reverseString(str) {
  return str
    .split("")
    .reverse()
    .join("");
}

function findLongestWordLength(s) {
  return s.split(' ')
    .reduce(function(x, y) {
      return Math.max(x, y.length)
    }, 0);
}

function findLongestWordLength(str) {
  return Math.max(...str.split(" ").map(word => word.length));
}

function largestOfFour(arr) {
  let a = [];
  arr.map( el => {
    a.push(Math.max(...el));
  });
  return a;
}

function largestOfFour(arr) {
  return arr.map(function(group) {
    return group.reduce(function(prev, current) {
      return current > prev ? current : prev;
    });
  });

  function largestOfFour(arr) {
  return arr.map(Function.apply.bind(Math.max, null));
}

function largestOfFour(arr, finalArr = []) {
  return !arr.length
    ? finalArr
    : largestOfFour(arr.slice(1), finalArr.concat(Math.max(...arr[0])))
}


function confirmEnding(str, target) {
  return str.slice(str.length - target.length) === target;
}
function confirmEnding(str, target) {
  let re = new RegExp(target + "$", "i");
  return re.test(str);
}
confirmEnding("He has to give me a new name", "name");


function repeatStringNumTimes(str, num) {
  var accumulatedStr = "";

  while (num > 0) {
    accumulatedStr += str;
    num--;
  }

  return accumulatedStr;
}
function repeatStringNumTimes(str, num) {
  return num > 0 ? str + repeatStringNumTimes(str, num - 1) : '';
}


function truncateString(str, num) {
  return str.length > num ? str.slice(0, num) + "..." : str;
}


function findElement(arr, func) {
 return arr.find(func);
}

findElement([1, 2, 3, 4], num => num % 2 === 0);



**don't mutate**

// the global variable
var bookList = ["The Hound of the Baskervilles", "On The Electrodynamics of Moving Bodies", "Philosophiæ Naturalis Principia Mathematica", "Disquisitiones Arithmeticae"];

/* This function should add a book to the list and return the list */
// New parameters should come before bookName
function add(list, bookName) {
  return [...list, bookName];
}

/* This function should remove a book from the list and return the list */
// New parameters should come before the bookName one
function remove(list, bookName) {
  return list.filter(book => book !== bookName);
}

var newBookList = add(bookList, 'A Brief History of Time');
var newerBookList = remove(bookList, 'On The Electrodynamics of Moving Bodies');
var newestBookList = remove(add(bookList, 'A Brief History of Time'), 'On The Electrodynamics of Moving Bodies');
console.log(bookList);
