# Fermetures (closures)
La fermeture est un comportement assez déroutant, qui est néanmoins compréhensible en connaissant le fonctionnement des contextes d'exécution. Cela apporte au langage des fonctionnalités très puissantes, comme de garder des variables privées (enregistrements d'état) et donc de mimer le comportement d'un objet à la façon d'un langage orienté objet, tout en gardant ses capacités fonctionnelles.

## Introduction

**Fermeture = Fonction + environnement lexical lors de sa définition**

![Closure](img/12a-closure.png)

Une fermeture est un mécanisme implicite ("automatique") qui permet à une fonction d'avoir accès aux variables de sa fonction parente (son environnement/portée lexical), même si celle-ci a déjà retourné un résultat. Le bon sens voudrait que les variables locales n'existent plus, mais elles existent encore. Cette liaison entre la fonction et son environnement préserve la chaîne de portée du ramasse miettes (garbage collector). C'est une référence stockée dans une propriété cachée [[Scope]] de la fonction. Cette référence est utilisée et copiée en tant qu'environnement parent à chaque fois que la fonction est exécutée.

Pour résumer : une fonction intérieure a toujours accès aux variables et paramètres (variables libres) de sa fonction extérieure, même si celle-ci a déjà retourné un résultat.

## Exemple "counter"

```javascript
// Exemple avec un compteur
function createCounter(start) { // Fonction parente
	// Closure :
	// les variables start et count sont capturées par la fonction anonyme retournée.
	let count = start; // Définit qu'une seule fois
	return function () {
		return ++count;
	}
}

/*
Autre écriture
const createCounter = start => {
	let count = start;
	return () => ++count;
}
*/

// myCounter fait référence à la définition de la fonction anonyme
// la closure permet à myCounter d'utiliser start ou count définis
// dans la fonction parente
const myCounter = createCounter(10);
myCounter(); // incrémentation de count : 11
myCounter(); // incrémentation de count : 12

// On peut créer une autre fonction
// son environnement lui est propre
const myCounter2 = createCounter(5);
myCounter2(); // incrémentation de count : 6
myCounter2(); // incrémentation de count : 7
```

![Execution Context Closure](img/12b-execution-context-closure.png)

## Autres explications

On dit qu'elle capture les variables libres de cette portée locale. Les variables libres sont des variables qui ne sont ni un paramètre, ni une variable locale à la fonction. Elles deviennent privées (encapsulation), protégées de changements externes et sont persistantes (mémoire des états).

> The key to remember is that **when a function gets declared, it contains a function definition and a closure**. The closure is a collection of all the variables in scope at the time of creation of the function. You may ask, does any function has a closure, even functions created in the global scope? The answer is yes. Functions created in the global scope create a closure. But since these functions were created in the global scope, they have access to all the variables in the global scope. And the closure concept is not really relevant. When a function returns a function, that is when the concept of closures becomes more relevant. The returned function has access to variables that are not in the global scope, but they solely exist in its closure.

> Only functions create scopes in global scope. After a function exists, if none of the contents of its scope are referenced by other (active) scopes then the function's scope is destroyed. Alternatively, **if any of the contents of an exited function's scope are referenced in other scopes, the function's scope will continue to live** on until no such references exist.


## Accès en références
Différent de scope. Continue d'être accessible après l'exécution de la fonction. Accès en références (bind), pas en valeurs. Read/write the variable, on peut modifier une variable globale !

```javascript
// Exemple : accès en références
let name = 'Bruce Wayne';
function mentalist() {
	console.log(`I know who you are ${name}!`);
}
name = 'Batman';
mentalist(); // I know who you are Batman!
```

## Utilisations

### Encapsulation
Données privées : Login / API key. On ne pollue pas la mémoire globale.

```javascript
// Explicit closure
function myBrain(code){
  const myCode = 12345;
  return function() {
		// Captured variables can be used here, in the inner function
		if (code === myCode) console.log('I am your puppet ...');
		else console.log('Wrong code. You can\'t access my brain!');
	}
}

const mentalist1 = myBrain(67890);
mentalist1(); // Wrong code. You can't access my brain!
const mentalist2 = myBrain(12345);
mentalist2(); // I am your puppet ...
```

## AOP (Aspect Oriented Programming)
Like **OOP principles** : self-contained, follows abstraction and encapsulation.

```javascript
// 1. Object with no privacy
const fakeHero = {
	energy: 100,
	addEnergy: function() { this.energy += 10; },
	getEnergy: function() { console.log(`Energy: ${this.energy}`); }
}

const superduck = fakeHero;
superduck.addEnergy();
superduck.getEnergy(); // Energy: 110
superduck.energy = 10000; // Bullshit!
superduck.getEnergy(); // Energy: 10000

// 2. More realistic: data privacy with closures
function trueHero(name) { // argument capturé

	// encapsulation : espace privé
	let _energy = 100; // variable capturée
	const _sendQuote = function() {
		let msg = '';
		if (_energy === 110) msg = 'Privacy is a function of liberty.';
		else msg = 'I would rather be without a state than without a voice.';
		console.log(msg);
	};

	// abstraction, interface
	return {
		// public variables and methods in an object
		name, // shortcut for "name: name,"
		addEnergy: function() { _energy += 10; _sendQuote(); },
		getEnergy: function() { console.log(`Energy: ${_energy}`); }
	}
}

const snowden = trueHero('Snowden'); // invoke function
snowden.addEnergy(); // Privacy is a function of liberty
snowden.getEnergy(); // Energy: 110
snowden._energy = 10000; // No effect, _energy is inside closure scope
snowden.getEnergy(); // Energy: 110
```

## Module pattern (namespace)
Module pattern : **IIFE** + **closures**. Independants, separation of concerns (make one thing independently). Toutes les librairies / framework utilisent ce pattern pour ne pas polluer le contexte global, pour préserver les noms et valeurs.
Declare-and-call protects your code and avoid globals. Avoids common problem with namespace/name collision

```javascript
const myModule = (function() {
    // #private aera
    let a = 100 ;
		let b = function(){ a += 10; };
    // Public object : methods
    return {
				addA: function() { b(); },
        getA: function() { console.log(`Private a is : ${a}`); }
    };
})();// IIFE

myModule.addA();
myModule.getA(); // Private a is : 110
```


## Once / singleton pattern
Limiter le nombre d'appel à une fonction à une seule fois.

Source : https://www.dofactory.com/javascript/singleton-design-pattern

```javascript
const Singleton = (function () {
		let instance;

		function createInstance() {
				const object = new Object("I am the instance");
				return object;
		}

		return {
				getInstance: function () {
						if (!instance) {
								instance = createInstance();
						}
						return instance;
				}
		};
})();

const instance1 = Singleton.getInstance();
const instance2 = Singleton.getInstance();
console.log("Same instance? " + (instance1 === instance2)); // true
```

### Currying
A curried function is a function that takes multiple parameters one at a time:

```javascript
const createAdd = a => b => a + b;
/*
Same as :
const createAdd = function(a) {
	return function (b) {
		return a + b;
	}
}
*/
const add5 = createAdd(5);
const add20 = createAdd(20);
add5(6); // 11
add20(7); // 27
```

You can read `const gte = cutoff => n => n >= cutoff` as: "gte is a function which takes cutoff and returns a function which takes n and returns the result of n >= cutoff".


### Asynchronous
console.dir()

```javascript
//implicit closure
const data ="My data!";
setTimeout(function() {
  console.log(data); // prints My data!
}, 3000);
```

```javascript
// Issue
let i;
for (i = 0 ; i < 5; i++) {
  const out = () => {
    console.log(i);  
  }
  setTimeout(out, 100);
}
```


### Factory pattern
> Define an interface for creating an object, but let subclasses decide which class to instantiate.

### Décorateur

### Shared scope

### Façade

### Call Count

### Mémoïsation

### Invocateur

### Throttling / Debouncing

### Queue fn (timers)

### Event timers

### Iterators/generator

### Event drive programming


## Issues
```javascript
var myAlerts = [];
for (var i = 0; i < 5; i++) {
    myAlerts.push(function inner() {
      alert(i);
    }
  );
}
myAlerts[0](); // 5
myAlerts[1](); // 5
myAlerts[2](); // 5
myAlerts[3](); // 5
myAlerts[4](); // 5
``












----
Liens :
- Vidéo : Javascript scope chain and closures
- http://davidshariff.com/blog/javascript-scope-chain-and-closures/
- https://developer.mozilla.org/fr/docs/Web/JavaScript/Une_r%C3%A9introduction_%C3%A0_JavaScript
- https://medium.com/dailyjs/i-never-understood-javascript-closures-9663703368e8
- http://doctrina.org/JavaScript:Why-Understanding-Scope-And-Closures-Matter.html
- https://www.slideshare.net/hymanroth/closures-in-javascript
- https://www.freecodecamp.org/news/deep-dive-into-scope-chains-and-closures-21ee18b71dd9/
- https://stackoverflow.com/questions/111102/how-do-javascript-closures-work
