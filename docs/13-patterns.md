# Patterns




https://www.geeksforgeeks.org/tail-recursion/


https://medium.com/javascript-scene/composing-software-the-book-f31c77fc3ddc


## constructor

function constructor(spec) {
	let {member} = spec;
	const {reuse} = other_constructor(spec);
	const method = function() {
		// spec, member, reuse, method
	};
	return Object.freeze({
		method,
		goodness: reuse.goodness
	});
}


## Site Reliability Engineering
https://landing.google.com/sre/books

## Composition
COMPOSITION vs INHERITANCE
https://medium.com/humans-create-software/composition-over-inheritance-cb6f88070205
inheritance is when you design your types around what they are, and composition is when you design types around what they do.
But the really big problem with inheritance is that you’re encouraged to predict the future. Inheritance encourages you to build this taxonomy of objects very early on in your project, and you are most likely going to make design mistakes doing that, because humans cannot predict the future (even though it feels like we can), and getting out of these inheritiance taxonomies is a lot harder than getting out of them.
**********************
dog            = pooper + barker
cat            = pooper + meower
cleaningRobot  = driver + cleaner
murderRobot    = driver + killer
murderRobotDog = driver + killer + barker
*********************
const barker = (state) => ({
  bark: () => console.log('Woof, I am ' + state.name)
})
const driver = (state) => ({
  drive: () => state.position = state.position + state.speed
})
barker({name:'karo'}.bark();
const murderRobotDog = (name)  => {
  let state = {
    name,
    speed: 100,
    position: 0
  }
  return Object.assign(
        {},
        barker(state),
        driver(state),
        killer(state)
    )
}
const bruno =  murderRobotDog('bruno')
bruno.bark() // "Woof, I am Bruno"










> Class inheritance can be used to construct composite objects, but it’s a restrictive and brittle way to do it. When the Gang of Four says “favor object composition over class inheritance”, they’re advising you to use flexible approaches to composite object building, rather than the rigid, tightly-coupled approach of class inheritance. They’re encouraging you to favor has-a and uses-a relationships over is-a relationships.

> Class inheritance is just one kind of composite object construction. All classes produce composite
objects, but not all composite objects are produced by classes or class inheritance. “Favor object
composition over class inheritance” means that you should form composite objects from small
component parts, rather than inheriting all properties from an ancestor in a class hierarchy. The
latter causes a large variety of well-known problems in object oriented design:
- The tight coupling problem
- The fragile base class problem
- The inflexible hierarchy problem
- The duplication by necessity problem
- The gorilla/banana problem: "...the problem with object-oriented languages is they’ve got all
this implicit environment that they carry around with them. You wanted a banana but what
you got was a gorilla holding the banana and the entire jungle." ∼ Joe Armstrong, “Coders at
Work”7


Function composition is the process of passing the return value of one function as an argument to another function. In mathematical notation: `f . g`, Which translates to this in JavaScript: `f(g(x))`

It’s evaluated from the inside out:
1. `x` is evaluated
2. `g()` is applied to `x`
3. `f()` is applied to the return value of `g(x)`


## memoize
https://scotch.io/tutorials/understanding-memoization-in-javascript
Memoization is a programming technique which attempts to increase a function’s performance by caching its previously computed results. Because JavaScript objects behave like associative arrays, they are ideal candidates to act as caches. Each time a memoized function is called, its parameters are used to index the cache. If the data is present, then it can be returned, without executing the entire function.  However, if the data is not cached, then the function is executed, and the result is added to the cache.
In the context of computer programs, the two major resources we have are time and memory. Thus, an expensive function call is a function call that consumes huge chunks of these two resources during execution due to heavy computation.

However, as with money, we need to be economical. For this, memoization uses caching to store the results of our function calls for quick and easy access at a later time.
