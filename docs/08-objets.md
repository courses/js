


source http://freecodecamp.org

object oriented programming organizes code into object definitions. These are sometimes called classes, and they group together data with related behavior. The data is an object's attributes, and the behavior (or functions) are methods.

The object structure makes it flexible within a program. Objects can transfer information by calling and passing data to another object's methods. Also, new classes can receive, or inherit, all the features from a base or parent class. This helps to reduce repeated code


Objects can have a special type of property, called a method.
Methods are properties that are functions. This adds different behavior to an object.



Anytime a constructor function creates a new object, that object is said to be an instance of its constructor. JavaScript gives a convenient way to verify this with the instanceof operator. instanceof allows you to compare an object to a constructor, returning true or false based on whether or not that object was created with the constructor
let Bird = function(name, color) {
  this.name = name;
  this.color = color;
  this.numLegs = 2;
}
let crow = new Bird("Alexis", "black");

**crow instanceof Bird**; // => true


**duck.hasOwnProperty(property)**


Since numLegs will probably have the same value for all instances of Bird, you essentially have a duplicated variable numLegs inside each Bird instance. This may not be an issue when there are only two instances, but imagine if there are millions of instances. That would be a lot of duplicated variables. A better way is to use Bird’s prototype. Properties in the prototype are shared among ALL instances of Bird
**Bird.prototype.numLegs = 2;**


You have now seen two kinds of properties: **own properties** and **prototype properties**. Own properties are defined directly on the object instance itself. And prototype properties are defined on the prototype
```javascript
function Bird(name) {
  this.name = name;  //own property
}
Bird.prototype.numLegs = 2; // prototype property
let duck = new Bird("Donald")
```
for (let prop in beagle) {
  if (beagle.hasOwnProperty(prop)) ownProps.push(prop);
  else prototypeProps.push(prop)
}


**Understand the Constructor Property**
There is a special constructor property located on the object instances duck and beagle
Note that the constructor property is a reference to the constructor function that created the instance. The advantage of the constructor property is that it's possible to check for this property to find out what kind of object it is.
```javascript
let duck = new Bird();
let beagle = new Dog();
console.log(duck.constructor === Bird);  // true
console.log(beagle.constructor === Dog);  // true
```

**hange the Prototype to a New Object**
A more efficient way is to set the prototype to a new object that already contains the properties. This way, the properties are added all at once:
```javascript
Bird.prototype = {
  numLegs: 2,
  eat: function() {
    console.log("nom nom nom");
  },
  describe: function() {
    console.log("My name is " + this.name);
  }
};
```

There is one crucial side effect of manually setting the prototype to a new object. It erases the constructor property! This property can be used to check which constructor function created the instance, but since the property has been overwritten, it now gives false results:

```javascript
duck.constructor === Bird; // false -- Oops
duck.constructor === Object; // true, all objects inherit from Object.prototype
duck instanceof Bird; // true, still works
```


```javascript
Bird.prototype = {
  constructor: Bird,
  numLegs: 2,
  eat: function() {
    console.log("nom nom nom");
  },
  describe: function() {
    console.log("My name is " + this.name);
  }
};
```

**isPrototypeOf**
Just like people inherit genes from their parents, an object inherits its prototype directly from the constructor function that created it. For example, here the Bird constructor creates the duck object:
duck inherits its prototype from the Bird constructor function. You can show this relationship with the isPrototypeOf method:

```javascript
function Bird(name) { this.name = name; }
let duck = new Bird("Donald");
Bird.prototype.isPrototypeOf(duck); // true
```

**Understand the Prototype Chain**
All objects in JavaScript (with a few exceptions) have a prototype. Also, an object’s prototype itself is an object. Because a prototype is an object, a prototype can have its own prototype! In this case, the prototype of Bird.prototype is Object.prototype:
```javascript
typeof Bird.prototype; // yields 'object'
Bird.prototype.isPrototypeOf(duck);  // yields true
Object.prototype.isPrototypeOf(Bird.prototype); // true
duck.hasOwnProperty("name"); // yields true
```

The hasOwnProperty method is defined in Object.prototype, which can be accessed by Bird.prototype, which can then be accessed by duck. This is an example of the prototype chain. In this prototype chain, Bird is the supertype for duck, while duck is the subtype. Object is a supertype for both Bird and duck. Object is a supertype for all objects in JavaScript. Therefore, any object can use the hasOwnProperty method.


**Use Inheritance So You Don't Repeat Yourself**
There's a principle in programming called Don't Repeat Yourself (DRY). The reason repeated code is a problem is because any change requires fixing code in multiple places. This usually means more work for programmers and more room for errors.


```javascript
function Animal() { }

Animal.prototype = {
  constructor: Animal,
  eat: function() {
    console.log("nom nom nom");
  }
};
/*
Bird.prototype = {
  constructor: Bird
};

Dog.prototype = {
  constructor: Dog
};*/
```


**Inherit Behaviors from a Supertype**
`Object.create(obj)` creates a new object, and sets obj as the new object's prototype. Recall that the prototype is like the "recipe" for creating an object. By setting the prototype of animal to be Animal's prototype, you are effectively giving the animal instance the same "recipe" as any other instance of Animal.

```javascript
// let animal = new Animal(); // some disadvantages
let animal = Object.create(Animal.prototype);
animal.eat(); // prints "nom nom nom"
animal instanceof Animal; // => true
```

**Set the Child's Prototype to an Instance of the Parent**
```javascript
Bird.prototype = Object.create(Animal.prototype);
let duck = new Bird("Donald");
duck.eat(); // prints "nom nom nom"

// When an object inherits its prototype from another object,
// it also inherits the supertype's constructor property.
duck.constructor // function Animal(){...}
Bird.prototype.constructor = Bird;
duck.constructor // function Bird(){...}
```

**Add Methods After Inheritance**
```javascript
Bird.prototype.fly = function() {
  console.log("I'm flying!");
};
let duck = new Bird();
duck.eat(); // prints "nom nom nom"
duck.fly(); // prints "I'm flying!"
```

**Override Inherited Methods**

```javascript
// Inherit all methods from Animal
Bird.prototype = Object.create(Animal.prototype);

// Bird.eat() overrides Animal.eat()
Bird.prototype.eat = function() {
  return "peck peck peck";
};
```

**Use a Mixin to Add Common Behavior Between Unrelated Objects**
As you have seen, behavior is shared through inheritance. However, there are cases when inheritance is not the best solution. Inheritance does not work well for unrelated objects like Bird and Airplane. They can both fly, but a Bird is not a type of Airplane and vice versa. For unrelated objects, it's better to use mixins. A mixin allows other objects to use a collection of functions.

```javascript
let flyMixin = function(obj) {
  obj.fly = function() {
    console.log("Flying, wooosh!");
  }
};
let bird = {
  name: "Donald",
  numLegs: 2
};

let plane = {
  model: "777",
  numPassengers: 524
};

flyMixin(bird);
flyMixin(plane);
bird.fly(); // prints "Flying, wooosh!"
plane.fly(); // prints "Flying, wooosh!"
```

**Use Closure to Protect Properties Within an Object from Being Modified Externally**
```javascript
function Bird() {
  let hatchedEgg = 10; // private variable

  /* publicly available method that a bird object can use */
  this.getHatchedEggCount = function() {
    return hatchedEgg;
  };
}
let ducky = new Bird();
ducky.getHatchedEggCount(); // returns 10
```


**IIFE**
```javascript
(function () {
  console.log("Chirp, chirp!");
})(); // this is an anonymous function expression that executes right away
// Outputs "Chirp, chirp!" immediately
```

**Use an IIFE to Create a Module**
The advantage of the module pattern is that all of the motion behaviors can be packaged into a single object that can then be used by other parts of your code.

```javascript
let motionModule = (function () {
  return {
    glideMixin: function(obj) {
      obj.glide = function() {
        console.log("Gliding on the water");
      };
    },
    flyMixin: function(obj) {
      obj.fly = function() {
        console.log("Flying, wooosh!");
      };
    }
  }
})();

motionModule.glideMixin(duck);
duck.glide();
```


----


At their most basic, objects are just collections of key-value pairs. In other words, they are pieces of data (values) mapped to unique identifiers called properties (keys). Take a look at an example:
In short, they are key-value stores which provide a flexible, intuitive way to structure data, and, they provide very fast lookup time


const tekkenCharacter = {
  player: 'Hwoarang',
  fightingStyle: 'Tae Kwon Doe',
  human: true
};
tekkenCharacter.origin = 'South Korea';
tekkenCharacter['hair color'] = 'dyed orange';
const eyes = 'eye color';
tekkenCharacter[eyes] = 'brown';

**delete**
delete foods.apples;


**check properties**
let users = {
  Alan: {
    age: 27,
    online: true
  },
  Jeff: {
    age: 32,
    online: true
  }
};
users.hasOwnProperty('Alan'); //true
'Alan' in users; //true


**iterate for ... in**
NOTE: Objects do not maintain an ordering to stored keys like arrays do; thus a key's position on an object, or the relative order in which it appears, is irrelevant when referencing or accessing that key.


```javascript
let users = {
  Alan: {
    age: 27,
    online: false
  },
  Jeff: {
    age: 32,
    online: true
  },
  Sarah: {
    age: 48,
    online: false
  },
  Ryan: {
    age: 19,
    online: true
  }
};
function countOnline(obj) {
  let result = 0;
  for (let user in obj) {
    // [square-bracket] notation must be used to call a variable property name.
    if (obj[user].online === true) {
      result++;
    }
  }
  return result;
}
console.log(countOnline(users));
```

**Generate an Array of All Object Keys with Object.keys()**
We can also generate an array which contains all the keys stored in an object using the `Object.keys()` method and passing in an object as the argument. This will return an array with strings representing each property in the object. Again, there will be no specific order to the entries in the array.




Objets globaux


https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux

Javascript has a class free object makeup, relying instead on objects inheriting properties directly from other objects - this is prototypal inheritance.


Objects are VariablesAt their most basic, objects are just collections of key-value pairs. In other words, they are pieces of data (values) mapped to unique identifiers called properties (keys). Take a look at an example:


Objects are mutable: They are addressed by reference, not by value.
Any JavaScript object can be converted to an array using Object.values():

JavaScript objects are containers for named values, called properties and methods.


Propriétés
Méthode : An object method is an object property containing a function definition.

Methods are actions that can be performed on objects.



Getter/setter
JavaScript can secure better data quality when using getters and setters.
Example 1 access fullName as a function: person.fullName().
Example 2 access fullName as a property: person.fullName.


The Object.defineProperty() method can also be used to add Getters and Setters:

Why Using Getters and Setters?

    It gives simpler syntax
    It allows equal syntax for properties and methods
    It can secure better data quality
    It is useful for doing things behind-the-scenes



    ## Objets
    // JavaScript's most important data type is the object.
    // An object is a collection of name/value pairs, or a string to value map.
    var book = {
    // Objects are enclosed in curly braces.
    topic: "JavaScript",
    // The property "topic" has value "JavaScript".
    fat: true
    // The property "fat" has value true.
    };
    // The curly brace marks the end of the object.


    Dans la programmation orientée objet classique, les objets sont des collections de données et de méthodes opérant sur ces données. Imaginons un objet personne avec les champs prénom et nom. Il y a deux manières d'afficher son nom complet : de la façon « prénom nom » ou de la façon « nom prénom ». À l'aide des fonctions et des objets vus précédemment, voici une manière de le faire :


    - syntaxe littérale d'objet  à la place de new Object();, se rapproche de JSON
    - fonction constructeur
    - classe ES6

    Ensemble de clés valeur, créé à la volée (!= classe, pas de constructeur/new)


    Object Literal : In plain English, an object literal is a comma-separated list of name-value pairs inside of curly braces. Those values can be properties and functions. Here’s a snippet of an object literal with one property and one function.
    No need to invoke constructors directly or maintain the correct order of arguments passed to functions. Object literals are also useful for unobtrusive event handling; they can hold the data that would otherwise be passed in function calls from HTML event handler attributes.


    An object literal is zero or more comma-separated name/value pairs surrounded by curly braces {}
    dot notation today.weather.morning or with square brackets today['month']
    Objects refer to each other, they don't hold duplicate copies of data


    Using typeof includes all properties in the prototype chain including functions
    To avoid inherited properties, use hasOwnProperty(type); which returns true if that property exists only in that object itself (not the chain)

    ```javascript
    // Object literal syntax VS fonction constructeur pour imposer une structure, les mêmes propriétes
    const vehicle = {
    	id: 1,
    	brand: 'Renault',
    	model: 'Clio',
    	year: 2009,
    	color: 'red',
    	honk: function() { // Méthodes. Ajouts de comportements. Propriété avec une fonction.
    		console.log('Tuuuuuuuuut !');
    	}
    };

    // Propriétés. Notation pointée
    vehicle.model = "Megane";
    vehicle.honk();

    // Propriétés
    vehicle["model"] = "Megane";


    // Afficher les clés d'un objet
    const res = Object.keys(vehicle);// Renvoie un tableau. Méthode statique sur la classe Object.
    res.map( (item) => console.log(`La clé est ${item}.`) );
    console.log();


    // Getter et Setter(ES5). Accesseurs et mutateurs
    Object.defineProperty(vehicle, 'summary',  {
    	get: function() {
    		return `Véhicule ${this.model} de l'année ${this.year}.`
    	},
    	set: function(value){
    		const values = value.split(',');
    		this.brand = values[0];
    		this.color = values[1]
    	}
    });

    // Get
    console.log(vehicle.summary);

    // Set
    vehicle.summary = 'Peugeot, noire';

    // Copie d'objets ES5
    const person = { firstName: 'Jo', lastName: 'Ny', job: 'Teacher'};
    const person2 = Object.assign( {}, person); // 2 arguments
    const person3 = Object.assign( {}, person, { firstName: 'Ju' } );
    const person4 = JSON.parse(JSON.stringigy(person)); // "clonage du pauvre"

    // Copie d'objets ES6
    const personES6 = {...person};
    ```

    .



#### Mixin

https://javascript.info/mixins
a mixin is a class containing methods that can be used by other classes without a need to inherit from it.
In other words, a mixin provides methods that implement a certain behavior, but we do not use it alone, we use it to add the behavior to other classes.

Mixin – is a generic object-oriented programming term: a class that contains methods for other classes.
Some other languages allow multiple inheritance. JavaScript does not support multiple inheritance, but mixins can be implemented by copying methods into prototype.
We can use mixins as a way to augment a class by adding multiple behaviors, like event-handling as we have seen above.
Mixins may become a point of conflict if they accidentally overwrite existing class methods. So generally one should think well about the naming methods of a mixin, to minimize the probability of that happening.





These object literals are similar to anonymous classes in other languages like Java.
var newobj = {
  var1: true,
  var2: "very interesting",
  method1: function () {
    alert(this.var1)
  },
  method2: function () {
    alert(this.var2)
  }
};
newobj.method1();
newobj.method2();





Write Concise Object Literal Declarations Using Object Property Shorthand

const getMousePosition = (x, y) => ({
  x: x,
  y: y
});
const getMousePosition = (x, y) => ({ x, y });




Use getters and setters to Control Access to an Object
```javascript
class Book {
  constructor(author) {
    this._author = author;
  }
  // getter
  get writer() {
    return this._author;
  }
  // setter
  set writer(updatedAuthor) {
    this._author = updatedAuthor;
  }
}
const lol = new Book('anonymous');
console.log(lol.writer);  // anonymous
lol.writer = 'wut';
console.log(lol.writer);  // wut
```
Notice the syntax used to invoke the getter and setter. They do not even look like functions. Getters and setters are important because they hide internal implementation details. Note: It is convention to precede the name of a private variable with an underscore (_). However, the practice itself does not make a variable private.
