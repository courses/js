## Synchronous/Asynchronous
Ex: Event, setTimeOut, ...
JavaScript is single-threaded, we can add asynchronous behavior using a Promise!




EVENT LOOP


**Promises**
Callback Hell : forme en triangle, plusieurs de niveaux d'appels asynchrones qui s'imbriquent > promises
Promises : créations (produicing) de plusieurs Promesses, puis chaining/consuming avec then/catch.


https://dev.to/lydiahallie/javascript-visualized-promises-async-await-5gke
When writing JavaScript, we often have to deal with tasks that rely on other tasks! Let's say that we want to get an image, compress it, apply a filter, and save it
The value of the PromiseStatus, the state, can be one of three values:
- ✅ fulfilled or resolved: The promise has been resolved. Everything went fine, no errors occurred within the promise 🥳
- ❌ rejected : The promise has been rejected. Argh, something went wrong..
- ⏳ pending: The promise has neither resolved nor rejected (yet), the promise is still pending.

The value of a promise, the value of [[PromiseValue]], is the value that we pass to the either the resolved or rejected method as their argument. There are built-in methods to get a promise's value :
- .then(): Gets called after a promise resolved.
- .catch(): Gets called after a promise rejected.
- .finally(): Always gets called, whether the promise resolved or rejected.


when you know that a promise will always resolve or always reject, you can write Promise.resolve or Promise.reject. we can chain as many .thens as we want: the result of the previous then callback will be passed as an argument to the next then callback!


Message Queue: Macrotask / Microtask

```javascript
console.log('Start');

setTimeout(() => {
	console.log('Timeout')
}, 0);

Promise.resolve('Promise').then(res => console.log(res));

console.log('End');
```




```javascript
const getIDs = new Promise((resolve, reject) => {
	setTimeout( () => {
		resolve([120,250,223,546]); // returns successful > then
		//reject([120,250,223,546]); // failed > catch
	}, 1500);
});

getIDs
.then(IDs => { // fullfilled state, get data from resolve
	console.log('then: ', IDs);
})
.catch(error => {
	console.log('catch: ', error);
});
```

## Async/Await

Instead of explicitly using the Promise object, we can now create asynchronous functions that implicitly return an object! This means that we no longer have to write any Promise object ourselves.

When encountering an await keyword, the async function gets suspended. ✋🏼 The execution of the function body gets paused, and the rest of the async function gets run in a microtask instead of a regular task!

the engine jumps out of the async function and continues executing the code in the execution context in which the async function got called:

Did you notice how async functions are different compared to a promise then? The await keyword suspends the async function, whereas the Promise body would've kept on being executed if we would've used then!


browser throttlingThere are a number of reasons why a timeout may take longer to fire than anticipated. This section describes the most common reasons.


to consume Promises, not creating

const getRecipes = async function(){ // runs in backgrouns
	const IDs = await getIDs; // block, stop the async function, utilisé que dans une async fn
	console.log (IDs);
	return recipe; // retourne une promesse
};

getRecipes().then(result => console.log(`The result is: ${result}`));

## AJAX
Asynchonous Javascript And XML

## Web API : fetch
they obey JS syntax rules

The behaviors behind them are controlled by the environment running the JS engine, but on the surface they definitely have to abide by JS to be able to play in the JS playground.

Same-origin policy

CORS : cross origin ... > crossorigin.me proxy CORS
https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

fetch('https://crossorigin.me/https://www.metaweather.com/api/location/2487956/')
.then(result => {
	console.log(result);
})
.catch(error => {
	console.log(error);
});



## Event loop
https://dev.to/lydiahallie/javascript-visualized-event-loop-3dif

Message Queue:
- (Macro)task 	setTimeout | setInterval | setImmediate
- Microtask 	process.nextTick | Promise callback | queueMicrotask

If both the call stack and microtask queue are empty, the event loop checks if there are tasks left on the (macro)task queue. The tasks get popped onto the callstack, executed, and popped off!
