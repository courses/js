# Portée (scope)
Dans un programme les variables, fonctions ou objets sont accessibles suivant leur emplacement dans le code source. On parle de **portée lexicale** ou de [portée statique](https://en.wikipedia.org/wiki/Scope_(computer_science)) car elle est définit au moment de l'écriture du code (author-time) et non de son exécution (run-time), comme les langages à portée dynamique (dynamic scope) moins majoritaires que sont Perl, Bash, Shell, ...

Les termes _Lexical Environment_ et _Lexical Scope_ sont utilisés de manière équivalente, car c'est bien l'environnement, comme vu précédemment, qui mémorise les variables et la relation avec les autres environnements parents (_outer_).

## Types
On distingue deux types de portée :
- une seule **portée globale** (global scope) créée avant toute exécution ;
- les **portées locales**, créées par les fonctions.

Si une variable est créée dans le contexte global, sa portée est globale et donc accessible à toutes les autres fonctions filles. Les variables globales sont rattachées à l'objet global `window` pour le navigateur.

![Scope](img/11a-scope.png)

## Chaîne de portée (scope chain)
La définition des variables est résolue en cherchant d'abord dans leur portée locale, bloc ou fonction, et si cela échoue en cherchant dans les blocs extérieurs ou parents jusqu'au contexte global. On parle de **chaîne de portée** (scope chain), puisque cette recherche parcours les blocs suivant une chaîne de références d'environnements lexicaux. L'accès aux variables ne se fait que dans un seul sens, du plus proche au plus lointain, ce qui offre une garantie de sécurité car une fonction extérieure ne devrait pas pouvoir modifier les données internes d'une fonction fille.

## Bloc vs function scope
![Scope](img/11ab-scope.png)

Trois possibilités pour déclarer les variables :
- `var` : obsolète, car son accessibilité est trop permissive, function scope ;
- `let` : à la place de var, car inaccessible depuis un bloc parent, bloc scope ;
- `const` : on ne peut pas donner de nouvelle valeur, bloc scope.

Avec `let` et `const`, la portée n'est plus seulement créée par une fonction mais aussi entre les accolades `{ ... }` : {}, if, for, switch, while, function(){}, ...

> When you declare a variable with the let keyword inside a block, statement, or expression, its scope is limited to that block, statement, or expression.
> objects (including arrays and functions) assigned to a variable using const are still mutable

```javascript
// A block scope may be just curly braces
{
  // undeclared variables are bound to the global object
  a = "black magic!";
  var b = "weird magic!";
  let c = "not magic";
  const d = "idem";
}
console.log(a); // "black magic!"
console.log(b); // "weird magic!"
console.log(c); // ReferenceError
console.log(d); // ReferenceError
```

De manière générale, il est préférable d'éviter les variables globales. Le `'strict mode'`, entre autres, permet d'avoir un comportement de var proche de let.

## Shadowing
Une variable dans un bloc peut avoir le même nom (identifiant, _identifier_) qu'une variable parente. Sa résolution locale l'emportera, on parle de _shadowing_.

```javascript
const a = "global";
function f1(){
  const a = "local f1";
  function f2() {
    const a = "local f2"; // Shadowing
    console.log(a);
  }
  f2(); // "local f2"
}
f1()
```

## Call Stack vs Scope
Ne pas confondre pile d'exécution/d'appel avec la portée.

```javascript
// Global scope
let a = 'global';
function first(){
  // Inner scope first : a & b
  let b = 'first';
  second(); // Call function
}
function second(){
  // Inner scope second : a & c
  let c = 'second';
  console.log(a, b, c); // ReferenceError : b is not defined
}
first();
```

![Scope](img/11c-scope-vs-execution-context.png)


---
Image :
- - https://dev.to/lydiahallie/javascript-visualized-scope-chain-13pd

Quelques liens :
- https://dev.to/sandy8111112004/javascript-introduction-to-scope-function-scope-block-scope-d11
- https://codeburst.io/js-scope-static-dynamic-and-runtime-augmented-5abfee6223fe
- https://scotch.io/tutorials/understanding-scope-in-javascript
- https://www.freecodecamp.org/news/deep-dive-into-scope-chains-and-closures-21ee18b71dd9/
- https://johnresig.com/apps/learn/#49
- https://medium.com/dailyjs/i-never-understood-javascript-closures-9663703368e8
- https://medium.com/@osmanakar_65575/javascript-lexical-and-dynamic-scoping-72c17e4476dd
- https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/scope-closures/README.md
