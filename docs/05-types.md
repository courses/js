

primitives : immutables
JS engine: Deux valeurs booléenes pointeront vers le même espace mémmoire


/autres
immutable/mutable
value/ref

JavaScript recognizes six primitive (immutable) data types: Boolean, Null, Undefined, Number, String, and Symbol (new with ES6) and one type for mutable items: Object. Note that in JavaScript, arrays are technically a type of object.


## Variables

Let is a mathematical statement that was adopted by early programming languages like Scheme and Basic.

It seems let was chosen most likely because it is found in so many other languages to define variables, such as BASIC, and many others

let as used in formalized mathematics (especially the writing of proofs) indicates that the current instance of a variable exists only for the scope of that logical idea. In the following example, x immediately gains a new identity upon entering the new idea (usually these are concepts necessary to prove the main idea) and reverts immediately to the old x upon the conclusion of the sub-proof. Of course, just as in coding, this is considered somewhat confusing and so is usually avoided by choosing a different name for the other variable.


https://dmitripavlutin.com/javascript-hoisting-in-details/


Javascript has dynamic typing vs static. Data types are automatically assigned to variables


déclaration, assignation opérateur =.
var a1, a2, a3; // sur une même ligne

**noms**
pas de nombres
pas de noms réservés
seul caractères spéciaux _ $ ??

**Mutations**
var name = 23;
name = 'John';
var age = 30;
var isMarried = true;

**Type Coercion**
console.log(`${name} is a ${age} years old. Is he married ? ${isMarried}`);

**Destructuring**

const [name, age] =




## Variables, Data, Type

Inside the computer’s world, there is only data. You can read data, modify
data, create new data—but that which isn’t data cannot be mentioned. All
this data is stored as long sequences of bits and is thus fundamentally alike


To be able to work with such quantities of bits without getting lost, we must
separate them into chunks that represent pieces of information. In a JavaScript
environment, those chunks are called values. Though all values are made of bits,
they play different roles. Every value has a type that determines its role. Some
10values are numbers, some values are pieces of text, some values are functions,
and so on.
To create a value, you must merely invoke its name.



What is a variable?

Defining data and then manipulating that data is the basis of programming. Without data, programming doesn't really have a purpose. In JavaScript, we store this data inside variables. Essentially, variables are just containers which hold reusable data. We do this using special reserved keywords built into JavaScript. For changeable variables, we use var or let, and for constant variables, we use const. let and const declarations come with additional restrictions on how the variable can be used.



Computer programs work by manipulating values, such as the number 3.14 or the text
“Hello World.” The kinds of values that can be represented and manipulated in a
programming language are known as types, and one of the most fundamental charac-
teristics of a programming language is the set of types it supports. When a program
needs to retain a value for future use, it assigns the value to (or “stores” the value in) a
variable. A variable defines a symbolic name for a value and allows the value to be
referred to by name. The way that variables work is another fundamental characteristic
of any programming language.




## Types

    Number
    BigInt (ES2015 ?) typeof 42n
    String
    Boolean
    Symbol (ES2015) typeof Symbol()
    Object
        Function
        Array
        Date
        RegExp
    null
    undefined

Enfin, il y a également quelques types natifs pour gérer les exceptions : Error.

Any JavaScript value that is not a number, a string, a boolean, or null or undefined is
an object. An object (that is, a member of the type object) is a collection of properties
where each property has a name and a value (either a primitive value, such as a number
or string, or an object). One very special object, the global object, is covered in §3.5,

An ordinary JavaScript object is an unordered collection of named values. The language
also defines a special kind of object, known as an array, that represents an ordered
collection of numbered values.


JavaScript defines another special kind of object, known as a function. A function is an
object that has executable code associated with it. A function may be invoked to run
that executable code and return a computed value. Like arrays, functions behave dif-
ferently from other kinds of objects, and JavaScript defines a special language syntax
for working with them. The most important thing about functions in JavaScript is that
they are true values and that JavaScript programs can treat them like regular objects.

In addition to the Array and Function classes, core JavaScript defines three
other useful classes. The Date class defines objects that represent dates. The RegExp
class defines objects that represent regular expressions (a powerful pattern-matching
tool described in Chapter 10). And the Error class defines objects that represent syntax
and runtime errors that can occur in a JavaScript program.

Javascript simple types:

    numbers (has object-like methods but they are immutable)
    strings (has object-like methods but they are immutable)
    booleans (has object-like methods but they are immutable)
    null
    undefined

All other values are objects including arrays and functions.




5 different data types that can contain values:
- string
- number
- boolean
- object
- function

There are 6 types of objects:
- Object
- Date
- Array
- String
- Number
- Boolean

And 2 data types that cannot contain values:
- null
- undefined




**5 types de données primitifs** :
 no properties or methods
 Primitive values are immutable (they are hardcoded and therefore cannot be changed).


https://www.youtube.com/watch?v=9ooYYRLdg_g&list=PLwXWsGY5ca8yN-nfd0j4Wh8RpmgGk83-Z
stored dans la stack. Accès rapide, mais limitée
les autres types : stored dans la heap. pour plus d'accès, de changements, de mémoires.


- number: var n = 0; même si décimal (integer or floating point)
- string: Séquence de caractères pour le texte. var s = "Bonjour" ou 'Bonjour' ou `...` (backticks, template strings, et respect de la tabulation, retour à la ligne);
- boolean: Logique vrai ou fausse. var isGood = true ou false;
- undefined: Variable qui n'a pas encore de valeur. var bidule; (déclarée mais pas initialisée)
- null: var v = null; (signifier explicitement que la variable ne pointe pas vers un objet, always means 'non-existent')
- symbol.

Javascript has dynamic typing vs static. Data types are automatically assigned to variables


onst primitif = 'je suis un string';
console.log(primitif.toUpperCase()); // "JE SUIS UN STRING"

On invoque bien la méthode toUpperCase() et cela fonctionne, pourtant notre type est primitif… Eh oui, c’est parce que JavaScript effectue automatiquement la conversion entre la primitive et l’objet String. La chaine est temporairement transformée en un objet String le temps du traitement, puis il est détruit. Cela s’applique bien évidemment aux autres types.



### typeof
The typeofoperator is not a variable. It is an operator. Operators ( + - * / ) do not have any data type.

typeof "John"                 // Returns "string"
typeof 3.14                   // Returns "number"
typeof NaN                    // Returns "number"
typeof false                  // Returns "boolean"
typeof [1,2,3,4]              // Returns "object"
typeof {name:'John', age:34}  // Returns "object"
typeof new Date()             // Returns "object"
typeof function () {}         // Returns "function"
typeof myCar                  // Returns "undefined" *
typeof null                   // Returns "object"




3. <a name="type">Vérification de Type (Courtesy jQuery Core Style Guidelines)</a>

	3.A Types réels

	* String :

		`typeof variable === "string"`

	* Number :

		`typeof variable === "number"`

	* Boolean :

		`typeof variable === "boolean"`

	* Object :

		`typeof variable === "object"`

	* Array :

		`Array.isArray(arrayObject)`
		(dans la mesure du possible)

	* null :

		`variable === null`

	* null ou undefined :

		`variable == null`

	* undefined :

		* Variable globale :

			* `typeof variable === "undefined"`

		* Variable locale :

			* `variable === undefined`

		* Propriétés :
			* `object.prop === undefined`
			* `object.hasOwnProperty( prop )`
			* `"prop" in object`


	JavaScript est un langage typé dynamiquement - ce qui peut être votre meilleur ami comme votre pire ennemi : respectez donc les 'types', en appliquant les règles ci-dessus.

**Opérateur unaire**
Vous pouvez vous prémunir de ce problème en utilisant la conversion de type avec l'opérateur unaire + ou - :

  foo = +document.getElementById("foo-input").value;
        ^ opérateur unaire + convertissant à sa droite l'opérande en "number"



You cannot use typeof to determine if a JavaScript object is an array (or a date).
You can check the constructor property to find out if an object is an Array
function isArray(myArray) {
  return myArray.constructor === Array;
}
function isDate(myDate) {
  return myDate.constructor === Date;
}



### Unary operators

Operators that use two values are called binary operators, while those that
take one are called unary operators.

### Conversion
JavaScript variables can be converted to a new variable and another data type:
    By the use of a JavaScript function
    Automatically by JavaScript itself


#### Strings

    16-bit character set and don't have character types.
    Backslashes (\) are used for escaping characters that could cause problems in strings.
    Strings are immutable.



    var name = 'Patrick O\'Brian'; // using a backslash in front of the apostrophe
    console.log('name:', name); // name: Patrick O'Brian



"3" + 4 + 5; // "345"

The global method String() can convert numbers to strings.
The Number method toString() does the same.

 Pour être plus précis, ce sont des séquences de caractères Unicode, chaque caractère étant représenté par un nombre de 16 bits. Cette nouvelle devrait être bien accueillie par toute personne qui a déjà eu affaire à des problèmes d'internationalisation.

Si vous voulez représenter un seul caractère, il suffit d'utiliser une chaîne qui contient un seul caractère.

Pour connaître la longueur d'une chaîne, utilisez sa propriété length :

As soon as we call one of these methods on a string, then JS automatically puts wrapper around the string and converts it from a primitive to an object.
"Hello".splice

#### Numbers
There is a single, 64-bit floating point number type.


The global method Number() can convert strings to numbers.
Number("3.14")    // returns 3.14
Number(" ")       // returns 0
Number("")        // returns 0
Number("99 88")   // returns NaN

parseFloat() 	Parses a string and returns a floating point number
parseInt() 	Parses a string and returns an integer

On peut également utiliser l'opérateur unaire + pour convertir des valeurs en nombres :

+ "42";   // 42
+ "010";  // 10
+ "0x10"; // 16

isNaN()
isInfinite();
isFinite():



Unfortunately, calculations with fractional numbers are generally not.
The important thing is to be aware of it and treat fractional digital numbers as
approximations, not as precise values.



Math.pow(2,53) // => 9007199254740992: 2 to the power 53
Math.round(.6) // => 1.0: round to the nearest integer
Math.ceil(.6) // => 1.0: round up to an integer
Math.floor(.6) // => 0.0: round down to an integer
Math.abs(-5) // => 5: absolute value
Math.max(x,y,z) // Return the largest argument
Math.min(x,y,z) // Return the smallest argument
Math.random() // Pseudo-random number x where 0 <= x < 1.0
Math.PI // π: circumference of a circle / diameter
Math.E // e: The base of the natural logarithm
Math.sqrt(3) // The square root of 3
Math.pow(3, 1/3) //The cube root of 3
Math.sin(0) //Trigonometry: also Math.cos, Math.atan, etc.
Math.log(10) //Natural logarithm of 10
Math.log(100)/Math.LN10 //Base 10 logarithm of 100
Math.log(512)/Math.LN2 //Base 2 logarithm of 512
Math.exp(3) //Math.E cubed
