# Programmation fonctionnelle



## Introduction

Functional programming is an approach to software development based around the evaluation of functions. Like mathematics, functions in programming map input to output to produce a result. You can combine basic functions in many ways to build more and more complex programs. The functional programming software development approach breaks a program into small, testable parts. Functional programming follows a few core principles:
- Functions are independent from the state of the program or global variables. They only depend on the arguments passed into them to make a calculation
- Functions try to limit any changes to the state of the program and avoid changes to the global objects holding data
- Functions have minimal side effects in the program


Functional programming is a style of programming where solutions are simple, isolated functions, without any side effects outside of the function scope. `INPUT -> PROCESS -> OUTPUT`

Functional programming is about:
1. Isolated functions - there is no dependence on the state of the program, which includes global variables that are subject to change
2. Pure functions - the same input always gives the same output
3. Functions with limited side effects - any changes, or mutations, to the state of the program outside the function are carefully controlled


Source: http://freecodecamp.org












----

To ensure your data doesn't change, JavaScript provides a function `Object.freeze` to prevent data mutation.


Functional programming is a foundational pillar of JavaScript, and immutability is a foundational
pillar of functional programming. You can’t fully understand functional programming without first
understanding immutability. This story may help. [1]
- Immutability: The true constant is change. Mutation hides change. Hidden change
manifests chaos. Therefore, the wise embrace history.
- Separation:If you try to perform effects and logic at the same time, you may create hidden side effects which cause bugs in the logic. Keep functions small. Do one thing at a time, and do it well.
- Composition:Plan for composition. Write functions whose outputs will naturally work as inputs to many other functions. Keep function signatures as simple as possible.
- Flow:Shared objects and data fixtures can cause functions to interfere with each other. Threads competing for the same resources can trip each other up. A program can be reasoned about and outcomes
predicted only when data flows freely through pure functions.


• Pure functions over shared state and side effects
• Immutability over mutable data
• Function composition over imperative flow control
• Generic utilities that act on many data types over object methods that only operate on their
colocated data
• Declarative over imperative code (what to do, rather than how to do it)
• Expressions over statement


## History
The Church Turing Thesis shows that lambda calculus (Alonzo Church) and the Turing machine are equivalent. Lambda calculus is a universal model of computation based on function application. A Turing machine is a universal model of computation that defines a theoretical device that manipulates symbols on a strip of tape. In lamda calculus, functions are king. Everything is a function, including numbers. Thinking of functions as the atomic building blocks (the legos from which we construct our creations) is a remarkably expressive and eloquent way to compose software.

There are three important points that make lambda calculus special:
- Functions are always anonymous
- Functions are always unary. currying.
- Functions are first-class, meaning that functions can be used as inputs to other functions, and
functions can return functions.

```javascript
const compose = (f, g) => x => f(g(x));
const double = n => n * 2;
const inc = n => n + 1;
const transform = compose(double, inc);
transform(3); // 8
```


## FP

1. Predictable. Pure & declarative
2. Safe. Immutable state.
3. Transparent. First class state.
4. Modular. Composable first class closures.


- **Pure functions**. mapping same input/same output. No side effects
- **Function composition** f(g(x))
- **Avoid shared state**. the timing and order of function calls don’t change the result of calling
the function. With impure functions, it’s impossible to fully understand what a function does unless you know the entire history of every variable that the function uses or affects
- **Avoid mutating state**. An immutable object is an object that can’t be modified after it’s created. even frozen objects are not immutable unless you walk the whole object tree and freeze every object property. data structures called trie
data structures (pronounced “tree”) which are effectively deep frozen (immutable, mori)
- **Avoid side effects**. any application state change that is observable outside the called function other than its return value. modifiy external variable, log, writing to the screen/file/network, triggering any external process, calling any other fonctions with side-effects
- **Declarative vs imperative**. Imperative programs spend lines of code describing the specific steps used to achieve the desired results — the flow control: _How to do things_. Declarative programs abstract the flow control process (the how gets abstracted away), and instead spend lines of code describing the data flow: _What to do_.


Imperative code frequently utilizes statements (instructions). A statement is a piece of code which performs some action. Examples of commonly used statements include for , if , switch , throw , etc.

Declarative code relies more on expressions. An expression is a piece of code which evaluates to
some value. Expressions are usually some combination of function calls, values, and operators which
are evaluated to produce the resulting value.


## Pure functions

A pure function is a function which:
1. Given the same input, will always return the same output.
2. Produces no side effects. it can’t alter any external state.

Pure functions are completely independent of outside state, and as such, they are immune
to entire classes of bugs that have to do with shared mutable state.Pure functions are also extremely independent — easy to move around, refactor, and reorganize in your code, making your programs more flexible and adaptable to future changes.

many sources of concurrency in JavaScript. API I/O, event listeners, web
workers, iframes, and timeouts can all introduce nondeterminism into your program
Math.random()
Date()
A pure function must not rely on any external mutable state, because it would no longer be
deterministic or referentially transparent.


### Mapping
Pure functions are all about mapping. Functions map input arguments to return values, meaning
that for each set of inputs, there exists an output. A function will take the inputs and return the
corresponding output
```
const double = x => x * 2;
console.log( double(5) ); // 10
```
In algebra, this means exactly the same thing as writing 4 , so any place you see f(2) you can
substitute 4 . This property is called referential transparency


## Functor
A functor data structure is a data structure that can be mapped over (e.g., [1,2,3].map(x => x *
2) ). In other words, it’s a container which has an interface which can be used to apply a function to
the values inside it. When you see the word functor, you should think “mappable”.

```
const double = n => n * 2;
const doubleMap = numbers => numbers.map(double);
console.log(doubleMap([2, 3, 4])); // [ 4, 6, 8 ]
```
----
- [1]: Composition Software
- https://lucasfcosta.com/2017/05/08/All-About-Recursion-PTC-TCO-and-STC-in-JavaScript.html
- https://www.youtube.com/watch?v=HvMemAgOw6I
