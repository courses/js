# LANGAGE
*****************


An expression is a chunk of code that evaluates to a value.
5 + 4
'Hello'

The value of an expression can be given a name. When you do so, the expression is evaluated first,
and the resulting value is assigned to the name.
const name = "Hkk"



évaluer

objet/propriétés/value

**method chaining**
arr.map(double).map(double); // [4, 8, 12]
**predicat**
A predicate is a function that returns a boolean value ( true or false ). The .filter() method takes
a predicate and returns a new list, selecting only the items that pass the predicate (return true ) to
be included in the new list:
const gte = cutoff => n => n >= cutoff;
[2, 4, 6].filter(gte(4)); // [4, 6]
**side effect**

**edge case**

**JavaScript Expressions**. An expression is a combination of values, variables, and operators, which computes to a value. The computation is called an evaluation.


**JavaScript keywords** are used to identify actions to be performed.

**JavaScript Identifiers**
Identifiers are names. In JavaScript, identifiers are used to name variables (and keywords, and functions, and labels). The rules for legal names are much the same in most programming languages. In JavaScript, the first character must be a letter, or an underscore ```(_)```, or a dollar sign ($). Subsequent characters may be letters, digits, underscores, or dollar signs.


## Syntaxe / Glossary / Lexical structure
The lexical structure of a programming language is the set of elementary rules that
specifies how you write programs in that language. It is the lowest-level syntax of a
language; it specifies such things as what variable names look like, the delimiter char-
acters for comments, and how one program statement is separated from the next.


https://www.codecademy.com/articles/glossary-javascript

var x, y, z;       // How to declare variables
x = 5; y = 6;      // How to assign values (assignment operator)
z = x + y;         // How to compute values (arithmetic operator)


The JavaScript syntax defines two types of values: Fixed values and variable values.
Fixed values are called **literals**. Variable values are called **variables**.

A literal is a data value that appears directly in a program. The following are all literals:

An **identifier** is simply a name. In JavaScript, identifiers are used to name variables and
functions and to provide labels for certain loops in JavaScript code


We define as literal a value that is written in the source code
In computer science, a literal is a notation for representing a fixed value in source code. Almost all programming languages have notations for atomic values such as integers, floating-point numbers, and strings, and usually for booleans and characters; some also have notations for elements of enumerated types and compound values such as arrays, records, and objects. An anonymous function is a literal for the function type.

In contrast to literals, variables or constants are symbols that can take on one of a class of fixed values, the constant being constrained not to change. Literals are often used to initialize variables, for example, in the following, 1 is an integer literal and the three letter string in "cat" is a string literal:



**déclaration**

**définition**


**expressions**

**statement**
If the phrases of JavaScript are expressions, then the full sentences are statements
an expression is something that computes a value but doesn’t
do anything: it doesn’t alter the program state in any way. Statements, on the other
hand, don’t have a value (or don’t have a value that we care about), but they do alter
the state.

If an expression corresponds to a sentence fragment, a JavaScript statement
corresponds to a full sentence. A program is a list of statements.



A fragment of code that produces a value is called an expression. Every value
that is written literally (such as 22 or "psychoanalysis" ) is an expression. An
expression between parentheses is also an expression, as is a binary operator
applied to two expressions or a unary operator applied to one.















********************

Fn First class:
- are objects
- can be created at runtime
- can be stored in variables
- parms fn
- returned by fn
- can be expressed by anonymous literal


les fn sont des valeurs comme les autres



First-class function: a function which can participate as a normal data: be stored in a variable, passed as an argument, or returned as a value from another function.

The use-case described above is known as the downwards funarg problem, i.e. an ambiguity at determining a correct environment of a binding: should it be an environment of the creation time, or environment of the call time?

This is solved by an agreement of using static scope, that is the scope of the creation time.

Def. 13: Static scope: a language implements static scope, if only by looking at the source code one can determine in which environment a binding is resolved.

The static scope sometimes is also called lexical scope, hence the lexical environments naming.

Technically the static scope is implemented by capturing the environment where a function is created.




This: an implicit context object accessible from a code of an execution context — in order to apply the same code for multiple objects
https://www.youtube.com/watch?v=KRpYZBUkUsk

http://dmitrysoshnikov.com/ecmascript/javascript-the-core-2nd-edition/#execution-context

## paradigme
The term "paradigm" in programming language context refers to a broad (almost universal) mindset and approach to structuring code. Within a paradigm, there are myriad variations of style and form that distinguish programs, including countless different libraries and frameworks that leave their unique signature on any given code.

But no matter what a program's individual style may be, the big picture divisions around paradigms are almost always evident at first glance of any program.

Typical paradigm-level code categories include procedural, object-oriented (OO/classes), and functional (FP):

    Procedural style organizes code in a top-down, linear progression through a pre-determined set of operations, usually collected together in related units called procedures.

    OO style organizes code by collecting logic and data together into units called classes.

    FP style organizes code into functions (pure computations as opposed to procedures), and the adaptations of those functions as values.




 first-class functions
 Javascript has a class free object makeup, relying instead on objects inheriting properties directly from other objects - this is prototypal inheritance.



 JavaScript is an object-oriented language. Loosely, this means that rather than having
 globally defined functions to operate on values of various types, the types themselves
 define methods for working with values. To sort the elements of an array a , for example,
 we don’t pass a to a sort() function. Instead, we invoke the sort() method of a :
 a.sort();
 // The object-oriented version of sort(a).



 JavaScript est un langage dynamique multi-paradigme : il dispose de différents types, opérateurs, objets natifs et méthodes. Sa syntaxe s'inspire des langages Java et C, donc de nombreuses structures de ces langages s'appliquent également à JavaScript. À la différence de ces langages, JavaScript n'a pas de classes. Au lieu de cela, la fonctionnalité des classes est reprise par les prototypes d'objet (voir notamment l'héritage et la chaîne de prototypes ainsi que le sucre syntaxique pour les Classes apparu avec ES6/ES2015). L'autre grande différence tient dans le fait que les fonctions sont des objets, on peut donc stocker ces fonctions dans des variables et les transmettre comme n'importe quel objet.


 JavaScript est un langage dynamique et ne possède pas de concept de classe à part entière (le mot-clé class a certes été ajouté avec ES2015 mais il s'agit uniquement de sucre syntaxique, JavaScript continue de reposer sur l'héritage prototypique


  Les objets sont créés par le programme et des méthodes et des propriétés lui sont attachés lors de l'exécution, contrairement aux définitions de classes courantes dans les langages compilés comme C++ et Java. Une fois qu'un objet a été construit, il peut servir de modèle (ou prototype) pour créer des objets similaires.



 S is a multi-paradigm language, meaning the syntax and capabilities allow a developer to mix and match (and bend and reshape!) concepts from various major paradigms, such as procedural, object-oriented (OO/classes), and functional (FP).
a prototype-based, multi-paradigm, , supporting object-oriented, imperative, and declarative (e.g. functional programming) styles. Read more about JavaScript.
dynamic language






## Le langage
Le langage présente des similitudes avec d'autres langages mais aussi certaines particularités qui génèrent quelques confusions mais qui en sont aussi la puissance.

’appartenance de certains mots à des langages aux paradigmes objets différents, allié à l’usage de certains termes à tort, est responsable pour une grande part de la mauvaise compréhension du modèle objet JavaScript.

**Scripts**
Langage de programmation sous formes de scripts (fichiers .js), interprété par les différents moteurs JavaScript présents dans les différents environnements (navigateurs, serveurs).

Les scripts sont utilisés pour raccourcir le processus traditionnel de développement édition-compilation-édition des liens-exécution propre aux langages compilés.

**Dynamique**
Typage faible et dynamique.

Le choix des types des données (nombre, texte, booléens, etc.) sont réalisés à la volée, lors de l'exécution du programme. A l'inverse le typage statique est inscrit en "dur" dans le code source et analysé par les outils de compilation.


**Multi-paradigme**
 Il existe 3 principaux paradigmes de programmation : procédural, orienté objet et fonctionnel. Si les deux premières sont très répandues la dernière ne connait du succès que depuis quelques années.






**Functional programming**
FP libraries : immutable.js, mori, underscore, lodash, ramda, ...

 is a style of programming where solutions are simple, isolated functions, without any side effects outside of the function scope.

INPUT -> PROCESS -> OUTPUT

Functional programming is about:

1) Isolated functions - there is no dependence on the state of the program, which includes global variables that are subject to change

2) Pure functions - the same input always gives the same output

3) Functions with limited side effects - any changes, or mutations, to the state of the program outside the function are carefully controlled



    Procedural style organizes code in a top-down, linear progression through a pre-determined set of operations, usually collected together in related units called procedures.

    OO style organizes code by collecting logic and data together into units called classes.

    FP style organizes code into functions (pure computations as opposed to procedures), and the adaptations of those functions as values.


 POO
 Le terme programmation orientée objet est partagé par le JS, basé sur des objets et dont l’héritage est prototypal et par les langages OO “classiques”, basés sur les classes.


 Héritage
 En POOP le concept est plus limpide si l’on parle de délégation plutôt que d’héritage (ce concept est expliqué en profondeur dans un autre article [en] de la série de K. Simpson). L’objet n’hérite pas des propriétés d’autres objets – nous n’avons pas vraiment de classes en JS, nous y reviendrons – dans le sens où il n’en contient pas une copie mais un lien vers un autre objet : son prototype.

L’idée de délégation prend tout son sens lorsque l’on comprend que notre objet délègue au prototype la responsabilité de trouver une propriété ou une méthode, s’il ne la possède pas lui-même.

Le mot clé "class" vient perturber encore

__Programmation fonctionnelle__

https://github.com/getify/Functional-Light-JS

https://mostly-adequate.gitbooks.io/mostly-adequate-guide/
__Des fonctions pures__. Un des principes de base de la PF est d’éviter les effets de bord. C’est à dire qu’une fonction ne doit pas modifier des choses en dehors de son propre scope (sa portée), tel qu’accéder ou modifier une variable extérieure à elle-même. Il s’agira de limiter ces fonctions impures au maximum afin de pouvoir facilement les identifier et de faciliter la lecture du code

__Immuabilité__
Dans l’approche fonctionnelle on préfère les valeurs immuables, c’est-à-dire celles qui ne peuvent pas être modifiées. Avec Javascript seuls les objest et les tableaux sont muables, pas les autres types de base. On ne crée pas un nouveau tableau en mémoire mais une autre référence pour le même tableau. La solution consiste à cloner nos tableaux et objets

Le JavaScript est un langage orienté objet à prototype et n’a pas vraiment été conçu pour l’immutabilité. En ES2018, on pourrait éviter de muter notre objet en utilisant le spread operator : const newUserRecord = {...user, loggedIn: true };

https://buzut.net/programmation-fonctionnelle-en-javascript/
https://buzut.net/programmation-orientee-objet-javascript/
https://codaholic.sillo.org/2019/12/28/maitriser-javascript-la-programmation-fonctionnelle/

// méthodes qui retournent une nouvelle valeur sans toucher à la variable originale
const merged = array.concat(arr1, arr2);
const filtered = array.filter(fn);
const mapped = array.map(fn);

// méthodes qui mutent la variable
array.push('newval');
array.pop();
array.sort();


__Fonction d’ordre supérieur__
    Les fonctions d’ordre supérieur sont des fonctions qui ont au moins une des propriétés suivantes :
    - elles prennent une ou plusieurs fonctions en entrée ;
    - elles renvoient une fonction.

En JavaScript, comme vous le savez certainement, une fonction est un objet comme un autre et il est très fréquent de passer des fonctions en argument, les callbacks en sont un des exemples les plus courants.
function hashPasswd(passwd, callback) {
    callback(computedHash);
}
hashPasswd(passwd, (hash) => {
}


 const l18n = {
    en: {
        hello: 'Hello',
        ciao: 'See you'
    },
    es: {
        hello: 'Hola',
        ciao: 'Hasta luego'
    },
    fr: {
        hello: 'Bonjour',
        ciao: 'À bientôt'
    }
};

function saySomethingToUser(l18n, locale, sayWhat) {
    return userName => `${l18n[locale][sayWhat]} ${userName}`;
}

const sayHelloInFrench = saySomethingToUser(l18n, 'fr', 'hello');
const sayCiaoInSpanish = saySomethingToUser(l18n, 'es', 'ciao');

console.log(sayHelloInFrench('Jean'));
console.log(sayCiaoInSpanish('Juan'));

__Composition de fonction__



__Currying__
Currying

Cette manière de retourner des fonctions permet très facilement de construire des fonctions plus spécifiques et d’éviter d’avoir à passer trop de paramètres. En effet, la fonction retournée “se souvient” des paramètres passés à la fonction parente, il s’agit d’une closure.        


    const values = [1, 5, 8]
    const result = values.map(element => element + 2)
    console.log(result) // [3, 7, 10]

La méthode map crée un nouveau tableau en appliquant à chaque élément du tableau en entrée la fonction passée en paramètre. On peut donc dire que c’est une fonction d’ordre élevé. On a de la même manière les méthodes filter et reduce.

On voit que les fonction d’ordre élevé ont le mérite d’éviter d’écrire des boucles. La programmation fonctionnelle se présente comme plutôt déclarative, on dit ce qu’on veut faire plutôt que comment le faire.





C'est un langage à objets utilisant le concept de prototype suivant plusieurs paradigmes de programmation : fonctionnelle, impérative et orientée objet.


 JavaScript s'appuyait sur un certain nombre de principes orienté objet tout en ayant un certain nombre de particularités


__Prototypes__
Objets à classes :

        Une classe définie par son code source est statique ;
        Elle représente une définition abstraite de l’objet ;
        Tout objet est instance d’une classe ;
        L’héritage se situe au niveau des classes.

Objets à prototypes :

        Un prototype défini par son code source est mutable ;
        Il est lui-même un objet au même titre que les autres ;
        Il a donc une existence physique en mémoire ;
        Il peut être modifié, appelé ;
        Il est obligatoirement nommé ;
        Un prototype peut être vu comme un exemplaire modèle d’une famille d’objet ;
        Un objet hérite des propriétés (valeurs et méthodes) de son prototype ;
