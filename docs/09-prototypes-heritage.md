### Prototypes
Avantage : une méthode partagé accessible par plusieurs objets et non copiée dans chacun d'eux.


Personne.prototype est un objet partagé par toutes les instances de Personne. Il fait partie d'une chaîne de résolution (qui a un nom spécial, la « chaîne de prototypes ») : chaque fois que vous essayez d'accéder à une propriété de Personne qui n'est pas définie, JavaScript va vérifier Personne.prototype pour voir si cette propriété n'existe pas plutôt à cet endroit. Par conséquent, tout ce qui est assigné à Personne.prototype devient disponible à toutes les instances de ce constructeur via l'objet this.

C'est un outil incroyablement puissant. JavaScript vous permet de modifier le prototype de quelque chose à tout moment dans votre programme, cela signifie qu'il est possible d'ajouter des méthodes supplémentaires à des objets existants lors de l'exécution :
Il est également possible d'ajouter des choses aux prototypes de classes d'objets JavaScript prédéfinies

La majorité des objets JavaScript sont des instances de Object qui est l'avant dernier maillon de la chaîne de prototype.

Les objets JavaScript sont des ensembles dynamiques de propriétés (les propriétés directement rattachées à un objet sont appelées propriétés en propre (own properties)). Les objets JavaScript possèdent également un lien vers un objet qui est leur prototype. Lorsqu'on tente d'accéder aux propriétés d'un objet, la propriété sera recherchée d'abord sur l'objet même, puis sur son prototype, puis sur le prototype du prototype et ainsi de suite jusqu'à ce qu'elle soit trouvée ou que la fin de la chaîne de prototype ait été atteinte.





Le temps de recherche des propriétés sera plus élevé si ces propriétés sont situées plus loin dans la chaîne de prototype.
Ainsi, quand on appelle :


En programmation orientée objet classique, les classes sont définies, puis lorsque des instances sont créées, l'ensemble des attributs et des méthodes sont copiés dans l'instance. En JavaScript en revanche, tout n'est pas copié : on établit un lien entre l'objet instancié et son constructeur (c'est un lien dans la chaîne de prototypage). On détermine alors les méthodes et les attributs en remontant la chaîne.

**attribut prototype et __proto__**

es éléments hérités sont ceux définis au niveau de l'attribut prototype d'Object (on peut voir cet attribut comme un sous espace de noms). Ainsi, les éléments listés sont ceux sous Object.prototype. et pas ceux situés juste sous Object. Ainsi Object.prototype.watch(), Object.prototype.valueOf() …  sont disponibles pour n'importe quel objet qui hérite de Object.prototype ce qui inclus les nouvelles instances créées à partir du constructeur Personne(). Object.is(), Object.keys(), ainsi que d'autres membres non définis dans prototype ne sont pas hérités par les instances d'objet ou les objets qui héritent de Object.prototype. Ces méthodes et attributs sont disponibles uniquement pour le constructeur Object().

il y a une différence entre la notion de prototype d'un objet (qu'on obtient via Object.getPrototypeOf(obj), ou via la propriété dépréciée  __proto__ ) et l' attribut prototyped'une fonction constructrice. La première concerne chaque instance, le dernier existe uniquement sur une fonction constructrice. Cela dit, Object.getPrototypeOf(new Foobar()) renvoie au même object queFoobar.prototype.

L'attribut prototype est un attribut qui contient un objet où l'on définit les éléments dont on va pouvoir hériter.


 Il ne s'agit donc pas d'une véritable instanciation au sens strict puisque JavaScript utilise un mécanisme différent pour partager des fonctionnalités entre les objets.

**attribut constructor**
L'attribut constructor renvoie vers la méthode constructrice utilisée

personne1.constructor.name


Le constructeur est l'équivalent JavaScript d'une classe. Il possède l'ensemble des fonctionnalités d'une fonction, cependant il ne renvoie rien et ne crée pas d'objet explicitement. Il se contente de définir les propriétés et les méthodes associées





----

En fait, on retrouve généralement la chose suivante : les attributs sont définis dans le constructeur, tandis que les méthodes sont définies au niveau du prototype. Cela rend le code plus simple à lire puisque les attributs sont groupés et les méthodes structurées en blocs distincts

// Constructeur avec définition des attributs
function Test(a, b, c, d) {
  // définition des attributs
};

// Définition de la première méthode
Test.prototype.x = function() { ... }

// Définition de la seconde méthode
Test.prototype.y = function() { ... }

---
var o = new Toto();
Le moteur JavaScript effectue à peu près  les étapes suivantes :

var o = new Object();
o.[[Prototype]] = Toto.prototype;
Toto.call(o);

Pour une priopriété
o.unePropriété;

Le moteur vérifie si o possède une propriété unePropriété en propre. Si ce n'est pas le cas, il vérifie Object.getPrototypeOf(o).unePropriété et ainsi de suite.





var s = "Simon";
s.inverse(); // TypeError on line 1: s.inverse is not a function

String.prototype.inverse = function inverse() {
  var r = "";
  for (var i = this.length - 1; i >= 0; i--) {
    r += this[i];
  }
  return r;
}
s.inverse(); // "nomiS"



Prototype d'une fonction est un objet créé automatiquement quand on crée une fonction.
Toutes les fonctions créées en JS ont automatiquement un prototype, dont la valeur par défaut est un objet vide qui lui est associé.
Si fn constructeur, new, le proto des instances (__proto__) pointent vers le même objet rattaché au même prototype.
Permet l'héritage.

Objet qui existe sur chaque fonction JS.
Propriété prototype. Ajoutée automatiquement.


// Ajout d'une propriété
Person.prototype.nationality = 'France'; // accessible à toutes les instances, héritage. Le prototype centralise les propriétés et méthodes de toutes les instances créées à l'aide du constructeur.
console.log(Person.prototype); // prototype de la fonction
console.log(p1.__proto__); // prototype de l'instance est le même.
console.log(p1.__proto__ === Person.prototype); // true, le prototype de l'instance pointe vers le même prototype de son constructeur
p1.nationality = "Spanish"; // ok dans Object.keys(p1), avant pas visible. Priorité de l'objet par rapport au prototype


// Ajout d'une méthode
Person.prototype.hello =  function(debut = 'Bonjour') {
	console.log(`${debut}, je m appelle ${this.name}. Je suis né en ${this.nationality}.`);
}

// Modifier le prototype après coup. Sera utilisé par la suite lors de la création des instances, à l'aide de la fonction utilisée comme constructeur
Person.prototype = { job: 'Teacher', adult: true };
const p2 = new Person('Jo', 1976);

// Les infos de __proto__ sont utiles pour éviter certains pièges
// Ex: le retour d'un "document.querySelectorAll()" donne une NodeList et non un Array. Pas les mêmes méthodes disponibles.



### Héritage

http://davidshariff.com/blog/javascript-inheritance-patterns/
Pseudoclassical pattern, Functional patterns, Prototypal pattern


En ce qui concerne l'héritage, JavaScript n'utilise qu'une seule structure : les objets. Chaque objet possède une propriété privée qui contient un lien vers un autre objet appelé le prototype. Ce prototype possède également son prototype et ainsi de suite, jusqu'à ce qu'un objet ait null comme prototype. Par définition, null ne possède pas de prototype et est ainsi le dernier maillon de la chaîne de prototype.

La majorité des objets JavaScript sont des instances de Object qui est l'avant dernier maillon de la chaîne de prototype.

Bien que cette confusion (entre classe et prototype) soit souvent avancée comme l'une des faiblesses de JavaScript, le modèle prototypique est plus puissant que le modèle classique et il est notamment possible de construire un modèle classique à partir d'un modèle prototypique.


var obj = {a: 1}; // obj ---> Object.prototype ---> nul
var arr = ['coucou', 'ça va', '?']; // arr ---> Array.prototype ---> Object.prototype ---> null
function fn() { return 2; } // fn ---> Function.prototype ---> Object.prototype ---> null

**Objets créés avec un constructeur**


**Objets créés avec Object.create()**

**Objets créés avec le mot-clé class**

// Chaîne d'héritage prototypal
// Tout hérite d'Object, à la racine
p2.__proto__.__proto__ ; // Object { ... }
p2.__proto__.__proto__.__proto__ ; // null


// ES5
function Teacher(name, year, school) {
	Person.call(this, name, year);
	this.school = school;
}
Teacher.prototype = Object.create(Person.prototype);
Teacher.prototype.constructor = Teacher;
const p3 = new Teacher('Jeremy', 1982, 'School of Arts');
p3.__proto__.__proto__.__proto__ ; // Object { ... }

// Test avec instanceof
p3 instanceof Teacher; // true
p3 instanceof Person; // true
p3 instanceof Object; // true

//ES6 : Classes
// syntaxic sugar : prototypal inheritance in JS. Plus simple.
class Person {
	constructor(name, year) {
		this.name = name;
		this.year = year;
	}
}

class Teacher extends Person {
	constructor(name, year, school) {
		super(name, year);
		this.school = school;
	}
}
