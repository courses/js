# INTRO

JavaScript est un langage de programmation, léger et multiplateforme. Il est utilisé dans différents environnements :
- côté client dans les navigateurs Web (Firefox, Chrome, IE / Edge, Opéra, Safari, ...) pour ajouter des comportements dynamiques aux sites Web (Front end).
- et aussi dans d'autres environnements comme Node.js (côté serveur ou Back end), Apache CouchDB, MongoDB, Adobe Acrobat, ...

Il fait partie des trois technologies principales du développement Web côté client ([image](img/00-html-js-css.jpg)).
- **HTML** pour le contenu : `<p>...</p>` signife "créer un paragraphe" ;
- **CSS** pour la présentation : `p { color: red; }` signifie "mettre le texte en rouge" ;
- **JS**  pour les effets dynamiques / programmation / applications web : `p.hide();` signifie "cacher le paragraphe"

Son histoire est singulière. Il aurait pu disparaître avec le Web dans les années 2000. Souvent décrié pour certains de ces comportements considérés comme peu intuitifs, il est aujourd'hui devenu incontournable.

> "**JavaScript est le langage de programmation le plus populaire et le plus incompris.**" [Douglas Crockford](https://www.crockford.com/javascript/javascript.html)

## Histoires

![JIT history](img/03a-jit.png)

> "[JS] n’a pas été conçu pour être rapide et en effet il ne l’était pas pendant les dix premières années. À partir de ce moment, la compétition entre navigateurs s’amplifia. En 2008, commença alors une période qu’on a appelée « la guerre de la performance ». Plusieurs navigateurs se sont dotés de compilateurs à la volée (ou « JIT » pour « Just In Time »). Lors de l’exécution de JavaScript, le compilateur JIT peut identifier différents motifs et rendre le code bien plus rapide grâce à ces derniers. L’introduction de ces compilateurs JIT a conduit à un point d’inflexion pour les performances de JavaScript. Son exécution est devenue 10 fois plus rapide. [...] **L’amélioration des performances a rendu possible l’utilisation de JavaScript pour traiter un tout nouvel ensemble de problèmes.**" [1]

Vidéo :
- [The Weird History of Javascript](https://dev.to/codediodeio/the-weird-history-of-javascript-2bnb).

Dates :
- 1995: [Brendan Eich](https://brendaneich.com/), un ingénieur de Netscape, crée en __[10 jours](https://brendaneich.com/2008/04/popularity/)__ le langage "Mocha" en s'inspirant les langages Scheme, Self et un peu Java. Il crée aussi le premier moteur Javascript SpiderMonkey. Mocha devient plus tard LiveScript.
- 1996: LiveScript devient JavaScript pour des raisons commerciales en se rapprochant de Java, mais rien à voir avec Java. Sortie de Netscape 2.
- 1997: Le comité [TC39](https://tc39.es/) au sein de l'Ecma International, une organisation de normalisation européenne sort une première édition du standard ECMAScript 1 (ES1). **JavaScript n'est depuis qu'une implémentation d'[ECMAScript](https://developer.mozilla.org/fr/docs/Web/JavaScript/Language_Resources)**, celle mise en œuvre par la fondation [Mozilla](https://developer.mozilla.org/fr/docs/Web/JavaScript). L'implémentation d'ECMAScript par Microsoft (dans Internet Explorer jusqu'à sa version 9) se nomme JScript, tandis que celle d'Adobe Systems se nomme ActionScript (utilisé anciennement dans Flash). Il est convenu de parler de **JS** ou **ES**, le nom JavaScript étant une marque déposée ... ([ECMA cloud](img/03b-ecma-cloud.png)).
- 1998: ES2, juste la version ISO d'ES1
- 1999: ES3, basée sur JS1.2 — closures, arrays, do-while, switch, try-catch, regular expressions, Unicode (UCS-2)
- 2000: "Déception du Web, certains en prédisent la chute. Microsoft abandonne le Web, les applets Java échouent." [2]
- 2005: La révolution Ajax (Asynchronous JavaScript + XML), **JS a une deuxième chance** (JS reboot). [Ajax: A New Approach to Web Applications (.pdf)](https://courses.cs.washington.edu/courses/cse490h/07sp/readings/ajax_adaptive_path.pdf). Ajax permet de faire des requêtes vers le serveur sans recharger la page, une nouvelle expérience utilisateur est possible, plus rapide, plus intuitive, conjointement à ce qu'on a appelé le [Web 2.0](https://fr.wikipedia.org/wiki/Web_2.0). Utilise pour google maps par exemple.
- 2006: John Resig développe la bibliothèque JQuery, qui facilite la programmation et devient très populaire.
- 2009: ES5, formellement ES3.1 — strict mode, JSON support, Array iteration, getters/setters, Object.defineProperty, ...
- 2015: [ES6/ES2015](https://www.w3schools.com/Js/js_es6.asp), sans doute la plus importante mise à jour — let, const, default parameters, Array.find/findIndex, iterators, for-of, modules, arrow functions, class, IIFE, Blocks, destructuring, Maps ... Bien supporté par la plupart des navigateurs. De fait c'est aujourd'hui le standard. Depuis, les mises à jour sont annuelles et les noms des versions reflètent l'année de validation "ES2015" par exemple.
- 2016-2019 : freeze, async/await, promises, rest / spread properties, ...

L'histoire de ce langage est aussi liée à d'autres aspects :
- le Web "social", le Web marchand et l'accès aux hauts débits ;
- les avancées d'HTML et de CSS, la fin des applets java et de Flash ;
- la montée en puissance des compilateurs à la volée (Just In Time, JIT compiler) dans les navigateurs ;
- le développement dans les navigateurs des [API Webs](https://developer.mozilla.org/en-US/docs/Web/API) accessibles en JS : DOM, video, fetch, WebGL, Canvas, localStorage, geolocation, WebRTC, Websockets, File, ... ;
- les outils de développements pour créer des applications Web (Web App, Rich Internet Application, Single Page Application) : frameworks JQuery / Angular / Vue / React, Babel, Webpack, ... ([image](img/01-js-ecosystem.jpg)) ;
- les utilisations côté serveur avec Node.js, pour que le langage de script soit le même que côté client ;
- "**installed by anyone, anywhere, on any device**" — unifier applications Web, de bureau (Desktop) et même mobiles (iOS, Android), avec les [Progressive Web Apps](https://web.dev/what-are-pwas/)(WPA) et Desktop WPA : Apache Cordova, Ionic, Web Assembly (WASM), NW.js, [Electron](https://github.com/sindresorhus/awesome-electron), Native scripts, React native, MEAN/JAM Stack, Nodebots.io, [ES4X](https://reactiverse.io/es4x/) / GraalVM / Vert.X, Emscripten, ...([image](img/02-beyond-the-browser.jpg)) ;
- le navigateur en tant que système d'exploitation : Chrome OS (ordinateurs chromebook), ChromiumOS, FireFox OS (projet abandonné).

![Reuse JS](img/02b-reuse.jpg)

<mark>Le déploiement multiplateforme et les performances des navigateurs proposent une vision séduisante : un langage de script pour de multiples cibles, applications, accessibles partout, avec peu d'installation.</mark>


## Voyage avec JS
Contrairement à la première impression, JS n'est pas facile à apprendre, il est déroutant. Les ressources disponibles oscillent entre petits programmes faciles mais peu utiles, et ressources techniques compliquées utilisant des termes de sciences informatiques. JS demande un véritable effort pour découvrir :
- son fonctionnement, les mécanismes qui régissent son exécution ;
- pour ceux venant des langages de programmation orientée objet (OOP), un nouveau paradigme de programmation, la programmation fonctionnelle et le rôle singulier joué par les fonctions et les objets.

La plupart de ces incompréhensions semble être lié au fait que la programmation orientée objet (orientée classe pourrait-on dire) est majoritaire. On calque nos habitudes sur un langage avec une approche différente. Puis quelques termes importants de vocabulaire communs à d'autres langages n'ont pas tout à fait les mêmes significations, ce qui rajoute à la confusion : object, class, ...

![FP vs OOP](img/05-fp-vs-oop-trends.png)

Dans ce voyage, il vaut mieux s'équiper :
- [MDN Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
- [JS The Right Way](https://jstherightway.org/)
- [W3schools](https://www.w3schools.com/js/default.asp)
- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS/)
- [FreeCodeCamp.org](https://www.freecodecamp.org/)
- [Eloquent JS](https://eloquentjavascript.net/)
- [FunFunFunctions channel](https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q)
- [Practical JavaScript](https://github.com/mjavascript/practical-modern-javascript)
- [Human JavaScript](https://read.humanjavascript.com/ch00-foreword.html)
- [Javascript 30 challenges](https://javascript30.com/)
- [How JavaScript Works](https://howjavascriptworks.com/)
- [How To Code in JavaScript](https://www.digitalocean.com/community/tutorial_series/how-to-code-in-javascript)
- [Awesome Javascript list](https://github.com/sorrycc/awesome-javascript)
- [JS apprendre à coder (fr)](https://www.pierre-giraud.com/javascript-apprendre-coder-cours)
- [JS Basics p5js](https://github.com/processing/p5.js/wiki/JavaScript-basics)
- Cours en ligne : khanacademy, udemy, udacity, codecademy, sx.codesmith.io, frontendmasters, whatchandcode, Nodeschool, Codeschool, ...


## Discussions
Beaucoup de discussions évoquent les aspects positifs et négatifs :
- [What the ... Javascript](https://www.youtube.com/watch?v=2pL28CcEijU), Kyle Simpsons
- [Really. JavaScript](https://www.youtube.com/watch?v=lTWGoL1N-Kc), Douglas Crockford (2010)

![Bad parts](img/04-bad-parts.jpg)

### Mauvais côtés
- Adoption trop rapide (web boom)
- Un a priori voudrait qu'étant un langage de "scripts", il ne serait pas un vrai langage de programmation, juste une suite d'instructions. _Pas ok_
- Parfois, on le définit comme "interprété" et on en déduit que ces performances sont beaucoup plus lentes qu'un langage compilé. _Pas ok_. Nous verrons plus loin que c'est plus compliqué.
- Beaucoup ont critiqué son "immaturité", certaines fonctionnalités classiques présentes dans d'autres langages étaient absentes. Chaque langage a ses particularités, on ne peut pas demander à JS d'avoir des types fixes, l'héritage multiple, des variables privées explicites, ... _moyennent ok_
- Certains aspects du langage peuvent être déroutants, difficilement prédictibles, "bad parts", "weird parts" : _ok_
  - variable globale, les valeurs de this, l'accès aux variables, la conversion dynamique des types (coercion), etc. C'est bien pour cela que les outils d'aide aux développeurs TypeScript, immutable.js, JSLint et ESLint existent.
  - [What the f*ck JavaScript?](https://github.com/denysdovhan/wtfjs)
  - [JavaScript Is Weird](https://www.youtube.com/watch?v=sRWE5tnaxlI)
  - [JavaScript: Understanding the Weird Parts](https://www.youtube.com/watch?v=Bv_5Zv5c-Ts)
  - [JavaScript: The Fairly Odd Parts](https://www.youtube.com/watch?v=jQPc6Usd44w).
  - [Javascript: The Good Parts (notes)](https://github.com/dwyl/Javascript-the-Good-Parts-notes)
  - [Life is Terrible: Let's Talk About the Web](https://vimeo.com/111122950)
- On peut reprocher à JS, comme au Web en général, de vouloir ringardiser les autres langages, pourtant indispensables, en une multitudes de modes, d'outils, de Frameworks. De fait, on a reproché l'utilisation abusive de JQuery pour des choses que l'on pouvait en JS natif (Vanilla) ([You don't need JQuery](https://blog.garstasio.com/you-dont-need-jquery/), ou [You might not need jQuery](http://youmightnotneedjquery.com/)). _moyennent ok_. Les frameworks ont aussi prouvé qu'en utilisant le langage d'une certaine manière, on pouvait harmoniser le code, fabriquer des sites complexes, proches des applications de bureau.

### Bons côtés

- Certains pensent tout simplement qu'il est mal compris, et qu'il faut revenir aux fondamentaux ;
- L'écosystème JS est riche, le langage peut servir de trait d'union (bindings, glue langage) entre plusieurs environnements : navigateurs, serveurs, etc. ;
- Communauté importante et très large, accessible au débutant et savoureux pour les experts (peut-être trompeur pour les débutants finalement) ;
- JS est le fruit d'un travail de normalisation entre différents acteurs. C'est compliqué de changer le Web... ;
- JS offre finalement plus de liberté pour la programmation, il n'impose pas de paradigme de programmation.

<mark>Malgré ses surprises et frustrations, JS nous donne la possibilité d'explorer une nouvelle façon de programmer/penser.</mark>

## Limitations (navigateurs)

- By default, JavaScript on a webpage is **restricted to read/write arbitrary files on the hard disk**, copy them or execute programs. It even has no direct access to OS system functions. Few modern browsers also allow it to work with files, but for the same, the access is limited and only provided if the user does certain actions, like “dropping” a file into a browser window. Some of the ways are also available to interact with camera/microphone and other devices, but that also requires user’s explicit permission. So a JavaScript-enabled page may not secretly enable a web-camera or may send the information to the remote server or any third party.
- Different tabs or windows generally do not know about each other instances. But technically that is very possible. The same is called "**Same Origin Policy**". For the same to work, both instances must contain some special JavaScript code that can handle data exchange between each other. But the limitation is again there for user’s safety. For example, a web page from URL abc.com which a user has opened must not be able to access another browser tab with the URL xyz.com and steal information secretly.
- With the use of JavaScript, one can easily communicate over the net to any remote server or third party where the current page came from. But for security reasons, its ability to receive or send data from other sites or domains is disabled. But the same is still possible, for which some JavaScript works require explicit agreement from the remote side. Such limitation will not be possible to provide, if JavaScript would be used outside of the browser rather than inside, i.e. either from a server. You may also notice that some of the modern day browsers allow installing plugin or extensions by which JavaScript works used inside the browser get extended permissions.
- It does not provide any functionality like multithreading or multiprocessor. [How JavaScript Works](https://javascript.info/intro).
- depends on DOM

---
- [1] [A cartoon intro to WebAssembly](https://tech.mozfr.org/post/2017/03/08/Une-introduction-cartoonesque-a-WebAssembly), Lin Clark (2017)
- [2] [Really. JavaScript](https://www.youtube.com/watch?v=lTWGoL1N-Kc), Douglas Crockford (2010)

Sources des images non mentionnées :
- Udemy : Modern Javascript, Build Real Projects
- [Frontend Monoliths: Run if you can!](https://www.slideshare.net/JonasBandi/frontend-monoliths-run-if-you-can-137391467)
- [Functional Programming - What Is It and Why Does It Matter?](https://www.keycdn.com/blog/functional-programming)
