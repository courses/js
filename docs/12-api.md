## Events (WEB API, message queue)
https://www.digitalocean.com/community/tutorials/understanding-events-in-javascript




Browser Object Model (BOM) that provides us to with the control on how applications are viewed on the browser. Some of the entities of BOM are:

Navigator

    History
    Location
    Screen
    Document
    Navigator



- Notifications that are sent to notify the code that something happened on the webpage : click a button, scrool, resize, key press, mouse, page loaded, submits a form...


**Event Handlers and Event Listeners**

Event Object
**Event Listener** : a function that performs an action based on a certain event. It waits for a specific event to happen

Problèmes avec this.
event delegation

Problèmes avec des paramètres

Ex: (modern browsers only)
element.addEventListener('click', toggleMenu.bind(null, param1, param2), false);

ou Ex:
element.addEventListener('click', function () {
  toggleMenu(param1, param2);
}, false);



## Service Workers

Service workers essentially act as proxy servers that sit between web applications, the browser, and the network (when available). They are intended, among other things, to enable the creation of effective offline experiences, intercept network requests and take appropriate action based on whether the network is available, and update assets residing on the server. They will also allow access to push notifications and background sync APIs.
