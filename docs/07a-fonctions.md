# Fonctions


## Définition


## terminology

**Callbacks** are the functions that are slipped or passed into another function to decide the invocation of that function. You may have seen them passed to other methods, for example in filter, the callback function tells JavaScript the criteria for how to filter an array.

Functions that can be assigned to a variable, passed into another function, or returned from another function just like any other normal value, are called **first class functions**. In JavaScript, all functions are first class functions.

The functions that take a function as an argument, or return a function as a return value are called **higher order functions**.

When the functions are passed in to another function or returned from another function, then those functions which gets passed in or returned can be called a **lambda**.


**imperative vs declarative**
In English (and many other languages), the imperative tense is used to give commands. Similarly, an imperative style in programming is one that gives the computer a set of statements to perform a task.

Often the statements change the state of the program, like updating global variables. A classic example is writing a for loop that gives exact directions to iterate over the indices of an array.

In contrast, functional programming is a form of declarative programming. You tell the computer what you want done by calling a method or function.


**mutation**
1) Don't alter a variable or object - create new variables and objects and return them if need be from a function.
The previous example didn't have any complicated operations but the splice method changed the original array, and resulted in a bug.

Recall that in functional programming, changing or altering things is called mutation, and the outcome is called a side effect. A function, ideally, should be a pure function, meaning that it does not cause any side effects.


**Pass Arguments to Avoid External Dependence in a Function**
2) Declare function arguments - any computation inside a function depends only on the arguments, and not on any global object or variable.
Another principle of functional programming is to always declare your dependencies explicitly. This means if a function depends on a variable or object being present, then pass that variable or object directly into the function as an argument.

There are several good consequences from this principle. The function is easier to test, you know exactly what input it takes, and it won't depend on anything else in your program.

This can give you more confidence when you alter, remove, or add new code. You would know what you can or cannot change and you can see where the potential traps are.

Finally, the function would always produce the same output for the same set of inputs, no matter what part of the code executes it.

## syntaxe

label, définition, signature, déclaration, Exécution

function outer () {...} : label + définition


What is a Function?
A function is a process which takes some input, called arguments, and produces some output called
a return value. Functions may serve the following purposes:
• Mapping: Produce some output based on given inputs. A function maps input values to output
values.
• Procedures: A function may be called to perform a sequence of steps. The sequence is known
as a procedure, and programming in this style is known as procedural programming.
• I/O: Some functions exist to communicate with other parts of the system, such as the screen,
storage, system logs, or network
[1] Composition Software


signature : functionName(param1: Type, param2: Type) => ReturnType


JavaScript has first class functions, which allows us to treat functions as data — assign them to
variables, pass them to other functions, return them from functions, etc


FIRST CLASS CITIZEN, HIGHER ORDER
Un langage de programmation fonctionnel doit me permettre de faire ceci :

  // Je stocke ma fonction dans une variable => first class citizen
  var f = function(){
    …
  };

  // Higher Order => ordre supérieur => reçoit des fonctions en paramètres
  function callIt(aFunction) {
    aFunction();
  }

  // Higher Order => ordre supérieur => renvoie des fonctions
  function create() {
    return function(param){
      …
    }
  }

  // Examples d’utilisation
  callIt(f); // fonction stockée précédemment dans f
  callIt(function(){
    …
  }); // Fonction anonyme crée à la volée
  create()(« Nice param »);g



  In computer programming, an anonymous function (function literal, lambda abstraction, or lambda expression) is a function definition that is not bound to an identifier.


##Lambda => λ => ‘fonction anonyme’.

Ce terme fait référence au lambda-calcul (λ-calcul), un système formel qui fonde les concepts de fonction et d’application de fonction à des arguments. Tout un univers gravitant autour du concept de fonction comme centre des préoccupations (approche plutôt étrange pour des OOP’istes, adeptes de la terre plate, pour qui une ‘fonction’ est avant tout une méthode, encapsulant un comportement d’instance/de classe).

Un univers très mathématiques, certes, mais qui énonce des règles et axiomes très puissants et intéressants pour qui souhaite en tirer parti dans le développement informatique


Voilà déjà un apport intéressant de la théorie ! La lambda calcul me dit que je peux considérer une fonction d’arité n comme autant de fonction d’arité 1, chacune étant une étape d’une application / exécution, plus complexe, partiellement résolue à chaque étape !

Quel intérêt me direz-vous ? J’étais moi aussi plutôt sceptique, au début. Mais nous verrons que dans certaines situations, cette propriété va nous être extrêmement utile pour arranger certaines tournures de style poussives en pirouettes fonctionnelles particulièrement élégantes.

Le terme officiel pour désigner ce passage (arité n) -> (n * arité 1) est ‘Curryfication‘ (ou ‘Schönfinkelisation’, mais c’est plus difficile à prononcer).

Quelle discipline, quels principes mettre en œuvre et respecter?

N’utiliser que des fonctions ‘pures’ : sans effets de bords
Ne pas utiliser de variables, uniquement des constantes
Ecrire ses programmes à partir de fonctions et, si possible, uniquement de fonctions
Les fonctions peuvent prendre en paramètres et renvoyer d’autres fonctions
‘Fonction’ est un type de donnée comme les autres (int, string…) et doit être utilisé comme tel.






HIgh order func


label define

function t(){}
const e = t; // changement de label


function t(){
  return function(){}
}
const e = t(); // retourne définition (code)


## Fonctions/prototypes
https://jscomplete.com/learn/lab-hofs/1lx9-functions-are-first-class-objects


In JavaScript functions are first-class objects - a function is a regular object of type function.
The ways of the declaration described above create the same function object type.
The function object type has a constructor: Function.
When Function is invoked as a constructor new Function(arg1, arg2, ..., argN, bodyString), a new function is created.


Paradigmes programmations.
Langage fonctionnel prototypal != Langage orienté objet (le plus important les objets, les classes : polymorphisme, héritages, ...)
Fonction "First class citizen". First Class Objects in Javascript
Les fonctions can be treated just like any object
Se sont des objets qui ont la particularité d'être appelée (invokable)



Fonction en tant que paramètres à des fonctions. Référence. High order fonction
Retourner des fonctions
Assignable à une variable
Array entries
Objets properties
Peut avoir des propriétés, y accéder


- A fn is an instance of the Object type
- A fn behaves like any other object
- We can store fn in a variable;
- We can pass a fn as an argument to another fn
- We can return a fn from a fn



Dans Javascript les fonction sont de « première classe » (rien à voir avec les trains), c’est juste pour dire qu’elles sont traitées comme n’importe quelle first-classautre variable :

    on peut assigner la fonction à une variable, mais également comme valeur dans un tableau ou un objet,
    on peut transmettre la fonction comme paramètre d’une autre fonction,
    on peut retourner la fonction à partir d’une autre fonction.





Si aucune instruction return n'est utilisée (ou que l'instruction return n'est suivie d'aucune valeur), JavaScript renvoie undefined.


https://dmitripavlutin.com/6-ways-to-declare-javascript-functions/





All Functions are Methods
In JavaScript all functions are object methods.
If a function is not a method of a JavaScript object, it is a function of the global object (see previous chapter).



Expression vs instruction (statement)


A function defined as the property of an object, is called a method to the object.
A function designed to create new objects, is called an object constructor.

**Functions are Objects**
The typeof operator in JavaScript returns "function" for functions.
But, JavaScript functions can best be described as objects.
JavaScript functions have both properties and methods.
The arguments.length property returns the number of arguments received when the function was invoked:




https://github.com/janosgyerik/javascript-notes/blob/master/functions.md




### Label / Définition / Exécution ()
https://youtu.be/ZVXrJ4dnUxM?t=906



- First, **a function definition can be stored in a variable**, the function definition is invisible to the program until it gets called.
- Second, **every time a function gets called, a local execution context is (temporarily) created**. That execution context vanishes when the function is done. A function is done when it encounters return or the closing bracket }.






### Rules
-  If the function uses this from the enclosing function, the arrow function is a good solution. When the callback function has one short statement, the arrow function is a good option too, because it creates short and light code.
- For a shorter syntax when declaring methods on object literals, the shorthand method declaration is preferable.
- new Function way to declare functions normally should not be used. Mainly because it opens potential security risks, doesn’t allow code auto-complete in editors and loses the engine optimizations.

### Noms, anonymes

https://ultimatecourses.com/blog/avoiding-anonymous-javascript-functions

```javascript
document.querySelector('.menu').addEventListener('click', function (event) {
  if (!this.classList.contains('active')) {
    this.classList.add('active');
  }
  event.preventDefault();
}, false);
```

More reusable, readable, debuggable :
// keep things outside the global scope
(function (window, document, undefined) {
  'use strict';

  // Selectors: caching
  var menu = document.querySelector('.menu');
  var users = document.querySelectorAll('.user');
  var signout = document.querySelector('.signout');

  // Methods
  function toggleMenu (event) {
    if (!this.classList.contains('active')) {
      this.classList.add('active');
    }
    event.preventDefault();
  }
  function showUsers (users) {
    for (var i = 0; i < users.length; i++) {
      var self = users[i];
      self.classList.add('visible');
    }
  }
  function signout (users) {
    var xhr = new XMLHttpRequest();
    // TODO: finish signout
  }

  // Events/APIs/init
  menu.addEventListener('click', toggleMenu, false);
  signout.addEventListener('click', signout, false);
  showUsers(users);

})(window, document);



### Déclaration de fonction
Function Literal

    A function literal has 4 parts:
        The (reserved) word function itself
        An optional name (un-named functions are considered anonymous functions)
        Comma-seperated parameters of the function, in parentheses - (parameters)
        Set of statements in curly brackets to be carried out when the function is invoked - {statements}

//Format of a function
function name (parameterA, parameterB){
	statements;
}

* Functions can be nested within functions and the inner function can access all the parameters of the outer function as well as its own



La déclaration function (ou l'instruction function) permet de définir une fonction et les paramètres que celle-ci utilise.

Par défaut, une fonction renvoie undefined. Pour renvoyer une autre valeur en résultat, une fonction doit utiliser une instruction return qui définit la valeur à retourner.


// Hoisted variable
console.log(hello('Aliens')); // => 'Hello Aliens!'
// Named function
console.log(hello.name)       // => 'hello'
// Variable holds the function object
console.log(typeof hello);    // => 'function'
function hello(name) {
  return `Hello ${name}!`;
}

The function declaration function hello(name) {...} create a variable hello that is hoisted to the top of the current scope. hello variable holds the function object and hello.name contains the function name: 'hello'.

in strictmode, Some JavaScript environments can throw a reference error when invoking a function whose declaration appears within blocks {...} of if, for or while statements.
if (true) {function ok() { return 'yes!';}}
console.log(ok()); // Throws "ReferenceError: ok is not defined"

As a general rule for these situations, when a function should be created by conditions - use a function expression.
let ok;
if (true) {ok = function() {return 'ouuu!';}}

**Regular Fonction**
The function declaration matches for cases when a regular function is needed. Regular means that you declare the function once and later invoke it in many different places.
Because the function declaration creates a variable in the current scope, alongside regular function calls, it is useful for recursion or detaching event listeners. Contrary to function expressions or arrow functions, that do not create a binding with the function variable by its name.

function factorial(n) {
  if (n === 0) {
    return 1;
  }
  return n * factorial(n - 1);
}
factorial(4); // => 24


### Expression de fonction
The function expression creates a function object that can be used in different situations:

    Assigned to a variable as an object count = function(...) {...}
    Create a method on an object sum: function() {...}
    Use the function as a callback .reduce(function(...) {...})


Une expression de fonction est très similaire et a presque la même syntaxe qu'une déclaration de fonction (consultez la page sur l'instruction function pour plus de détails). La différence principale entre une expression de fonction et une instruction est le nom de la fonction. En effet, pour les expressions, celui peut être omis (on parle alors d'une fonction anonyme).

En JavaScript, les expressions de fonction ne sont pas remontées (à la différence des déclarations de fonction). Il est donc impossible d'utiliser les expressions de fonction avant leur définition :


remontée(); // affiche "toto" dans la console
function remontée() {
  console.log("toto");
}

On notera que les expressions de fonctions ne sont pas remontées :

nonRemontée(); // TypeError: nonRemontée is not a function
var nonRemontée = function() {
   console.log("truc");
};


**Named**
function funName(variable) {...} is a named function expression. The variable funName is accessible within function scope, but not outside. Either way, the property name of the function object holds the name: funName.

When the expression has the name specified, this is a named function expression. It has some additional properties compared to simple function expression:

    A named function is created, i.e. name property holds the function name
    Inside the function body a variable with the same name holds the function object


const getType = function funName(variable) {
  console.log(typeof funName === 'function'); // => true
  return typeof variable;
}
console.log(getType(3));     // => 'number'
console.log(getType.name);   // => 'funName'

console.log(typeof funName); // => 'undefined'



It is reasonable to favor named functions and avoid anonymous ones to gain benefits like:

    The error messages and call stacks show more detailed information when using the function names
    More comfortable debugging by reducing the number of anonymous stack names
    The function name says what the function does
    You can access the function inside its scope for recursive calls or detaching event listeners


### Shorthand method definition

Shorthand method definition can be used in a method declaration on object literals and ES2015 classes.
The short approach of method definition has several benefits over traditional property definition with a name, colon : and a function expression add: function(...) {...}:

    A shorter syntax is easier to understand
    Shorthand method definition creates a named function, contrary to a function expression. It is useful for debugging.

Ex: objet literal
const collection = {
  items: [],
  add(...items) {
    this.items.push(...items);
  },
  get(index) {
    return this.items[index];
  }
};
collection.add('C', 'Java', 'PHP');
collection.get(1) // => 'Java'

**The class syntax requires method declarations in a short form:**
Ex: Classes
class Star {
  constructor(name) {
    this.name = name;
  }
  getMessage(message) {
    return this.name + message;
  }
}
const sun = new Star('Sun');
sun.getMessage(' is shining') // => 'Sun is shining'

**shorthand method declarations with computed property names.**
in object literals and classes.

Ex:
const addMethod = 'add',
	getMethod = 'get';

const collection = {
  items: [],
  [addMethod](...items) {
    this.items.push(...items);
  },
  [getMethod](index) {
    return this.items[index];
  }
};
collection[addMethod]('C', 'Java', 'PHP');
collection[getMethod](1) // => 'Java'


## ARROW FN

>In JavaScript, we often don't need to name our functions, especially when passing a function as an argument to another function. Instead, we create inline functions. We don't need to name these functions because we do not reuse them anywhere else.



https://dmitripavlutin.com/javascript-arrow-functions-best-practices/

Une expression de fonction fléchée (arrow function en anglais) permet d’avoir une syntaxe plus courte que les expressions de fonction et ne possède pas ses propres valeurs pour this, arguments, super, ou new.target. Les fonctions fléchées sont souvent anonymes et ne sont pas destinées à être utilisées pour déclarer des méthodes


var a = [
  "We're up all night 'til the sun",
  "We're up all night to get some",
  "We're up all night for good fun",
  "We're up all night to get lucky"
];

// Sans la syntaxe des fonctions fléchées
var a2 = a.map(function (s) { return s.length });
// [31, 30, 31, 31]

// Avec, on a quelque chose de plus concis
var a3 = a.map( s => s.length);
// [31, 30, 31, 31]

Les fonctions fléchées ne possèdent pas de prototype :




Let’s now shift our attention to the following example. What do you think the console.log statement will print?

function puzzle() {
  return function () {
    console.log(arguments)
  }
}
puzzle('a', 'b', 'c')(1, 2, 3)

The answer is that arguments refers to the context of the anonymous function, and thus the arguments passed to that function will be printed. In this case, those arguments are 1, 2, 3.

What about in the following case, where we use an arrow function instead of the anonymous function in the previous example?

function puzzle() {
  return () => console.log(arguments)
}
puzzle('a', 'b', 'c')(1, 2, 3)


Implicitly Returning Object Literals

When you need to implicitly return an object literal, you’ll need to wrap that object literal expression in parentheses. Otherwise, the compiler would interpret your curly braces as the start and the end of the function block.

var objectFactory = () => ({ modular: 'es6'

https://github.com/mjavascript/practical-modern-javascript/blob/master/ch02.asciidoc

Arrow functions give access to their defining environment while regular functions give access to their calling environment.
- The value of the this keyword inside a regular function depends on HOW the function was CALLED.
- The value of the this keyword inside an arrow function depends on WHERE the function was DEFINED.

La flèche utilisée pour une fonction fléchée n’est pas un opérateur. Les fonctions fléchées ont des règles spécifiques quant à leur place dans la syntaxe et interagissent différemment de la précédence des opérateurs par rapport à une fonction classique


Hérite de la valeur this de son parent

-
- () =>
- el => el.value
- (el, val) => {
	return;
}



var fonction = () => { toto: 1 }; // fonction() renverra undefined !
// l’analyse de l’expression trouve des blocs d’instructions au lieu de littéraux objets. Pour éviter cet effet indésirable, on pourra encadrer le littéral objet
var fonction = () => ({ toto: 1 }); // ok!





const addX = x => n => n + x
equivalent de : function addX(x) {
  return function(n) {
     return n + x
  }
}



ECMAScript 2015 improves this usage by introducing the arrow function, which takes the context lexically (or simply uses this from the immediate outer scope). This is nice because you don’t have to use .bind(this) or store the context var self = this when a function needs the enclosing context.


### Generator function

Function declaration form function* <name>():
Function expression form const a = function* ():
Shorthand method definition form __*<name>()__:



https://dev.to/lydiahallie/javascript-visualized-generators-and-iterators-e36
Normal functions follow something called a run-to-completion model: when we invoke a function, it will always run until it completes (well, unless there's an error somewhere). We can't just randomly pause a function somewhere in the middle whenever we want to. Now here comes the cool part: generator functions don't follow the run-to-completion model!

Generator functions actually work in a completely different way compared to regular functions:
- Invoking a generator function returns a generator object, which is an iterator : [... ] syntax, for of loop
- We can use the yield keyword in a generator function to "pause" the execution.

One of the biggest advantages of generators is the fact that they are lazily evaluated. This means that the value that gets returned after invoking the next method, is only computed after we specifically asked for it! Normal functions don't have this: all the values are generated for you in case you need to use it some time in the future.

**Iterators**
like arrays, strings, maps, and sets. It's actually because they implement the iterator protocol: the [Symbol.iterator].
[Symbol.iterator] has to return an iterator, containing a next method which returns an object just like we saw before: { value: '...', done: false/true }.


### IIFE
pour Immediately Invoked Function Expression ou expression de fonction immédiatement appelée
immediatly invoked function expression
avantage : ne pas polluer la portée globale avec les variables des fonctions, même avec var
data privacy avant ES6
module pattern

(function(){
  console.log('IIFE starts');
  var localIIFE = 'créée dans la IIFE';
  console.log(localIIFE); // ok
}());

console.log(localIIFE); > ReferenceError: localIIFE is not defined

### Fn constructeur

https://addyosmani.com/resources/essentialjsdesignpatterns/book/#constructorpatternjavascript

Constructors are functions that create new objects. They define properties and behaviors that will belong to the new object. Think of them as a blueprint for the creation of new objects.

- Constructors are defined with a capitalized name to distinguish them from other functions that are not constructors.
- Constructors use the keyword this to set properties of the object they will create. Inside the constructor, this refers to the new object it will create.
- Constructors define properties and behaviors instead of returning a value as other functions might.


Notice that the new operator is used when calling a constructor. This tells JavaScript to create a new instance of Bird called blueBird. Without the new operator, this inside the constructor would not point to the newly created object, giving unexpected results.


première lettre majuscule (standard d'écriture mais libre de faire ce que l'on veut)
imposer une structure à des objets
this
new


function Person(name, year) { this.name = name; this.year = year;}
const p1 = new Person('Bob', 1980);


__new__
Lorsque le code new Toto(...) est exécuté, voici ce qui se passe :

    Un nouvel objet est créé qui hérite de Toto.prototype.
    La fonction constructrice Toto est appelée avec les arguments fournis, this étant lié au nouvel objet créé. new Toto sera équivalent à new Toto() (i.e. un appel sans argument).
    L'objet renvoyé par le constructeur devient le résultat de l'expression qui contient new. Si le constructeur ne renvoie pas d'objet de façon explicite, l'objet créé à l'étape 1 sera utilisé. (En général, les constructeurs ne renvoient pas de valeurs mais si on souhaite surcharger le processus habituel, on peut utiliser cette valeur de retour).




    Portée d'une variable


    HOF : Fonction en tant que paramètres à des fonctions. Référence/

    A higher order function is any function which takes a function as an argument, returns a function,
or both. Higher order functions are often used to:
• Abstract or isolate actions, effects, or async flow control using callback functions, promises,
monads, etc.
• Create utilities which can act on a wide variety of data types
• Partially apply a function to its arguments or create a curried function for the purpose of reuse
or function composition
• Take a list of functions and return some composition of those input functions


    function isOdd(e) { return e % 2 === 1;}
    const newArray = [1, 2, 3].filter(isOdd);
    display.log(newArray);


    const add = (a, b) => a + b;
    function host(func) {
      return func;
    }
    const result = host(add)(40,2);


    **Notation raccourcie**
    Le code suivant :
    var obj = {
      toto: function() {},
      truc: function() {}
    };

    Peut désormais être raccourci en :
    var obj = {
      toto() {},
      truc() {}
    };


    var bar = {
      toto0 : function (){return 0;},
      toto1(){return 1;},
      ["toto" + 2](){return 2;},
    };

    console.log(bar.toto0()); // 0
    console.log(bar.toto1()); // 1
    console.log(bar.toto2()); // 2


    ## Générateurs

    https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/function*
    Les générateurs sont des fonctions qu'il est possible de quitter puis de reprendre. Le contexte d'un générateur (les liaisons avec ses variables) est sauvegardé entre les reprises successives.

    Les générateurs, combinés avec les promesses, sont des outils de programmation asynchrones puissants qui permettent de réduire les inconvénients causés par les callbacks (fonctions de rappel) et l'inversion de contrôle.

    ### Retours

    returns sinon undefined


    If possible, JavaScript will automatically insert a semicolon at the end of the line which the return statement is on. Use brackets to make it impossible.


    return (
        ...
    ) // <-- JavaScript inserts semicolon here


    function doSomething() {
        return

        // Valid, but will never be called
        doSomethingElse()
    }
