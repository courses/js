
## Workflow

/src/js/models
/src/js/views
/dist/
..

NODE/NPM/WEBPACK/BABEL/

Node :
npm init
npm install webpack  webpack-cli html-webpack-plugin--save-dev
sudo npm install live-server --global
fichiers : webpack.config.js
package.json!
"scripts": {
    "dev": "webpack"
    start
    build
  },

npm run dev

npm install --save-dev @babel/core @babel/preset-env babel-loader npm install --save core-js@3 regenerator-runtime
## Outils de développement
- caniuse.com
- https://kangax.github.io/compat-table/es6/
- glitch.com
- codepen
- jsFiddle
- JsBin
- JSLint
- ESLint
- babeljs.io
- https://jshint.com/
- https://gruntjs.com/
- https://mochajs.org/


Source [You Don't Know JavaScript](https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/get-started/ch1.md) :
**Developer Tools** are... tools for developers. Their primary purpose is to make life easier for developers. They prioritize DX (Developer Experience). It is not a goal of such tools to accurately and purely reflect all nuances of strict-spec JS behavior. As such, there's many quirks that may act as "gotchas" if you're treating the console as a pure JS environment. [...] think of the console as a "JS-friendly" environment. That's useful in its own right. [...]

One of the most foundational principles that guides JavaScript is preservation of **backwards compatibility**. Many are confused by the implications of this term, and often confuse it with a related but different term: forwards compatibility. Backwards compatibility means that once something is accepted as valid JS, there will not be a future change to the language that causes that code to become invalid JS. Code written in 1995—however primitive or limited it may have been!—should still work today. As TC39 members often proclaim, "we don't break the web!"  [...]

For new and incompatible syntax, the solution is **transpiling**. Transpiling is a contrived and community-invented term to describe using a tool to convert the source code of a program from one form to another (but still as textual source code). Typically, forwards-compatibility problems related to syntax are solved by using a transpiler (the most common one being Babel (https://babeljs.io)) to convert from that newer JS syntax version to an equivalent older syntax. [...] Developers should focus on writing the clean, new syntax forms, and let the tools take care of producing a forwards-compatible version of that code that is suitable to deploy and run on the oldest-supported JS engine environments. [...]

If the forwards-compatibility issue is not related to new syntax, but rather to a missing API method that was only recently added, the most common solution is to provide a definition for that missing API method that stands in and acts as if the older environment had already had it natively defined. This pattern is called a **polyfill (aka "shim")**.



## Bonnes pratiques

Précautions et bonnes pratiques :
- [Airbnb JavaScript Style Guide()](https://github.com/airbnb/javascript)
- [W3C JS Best Practices](https://www.w3schools.com/js/js_best_practices.asp)
- [Google Style Guide](https://google.github.io/styleguide/jsguide.html)
- [Idiomatic.js](https://github.com/rwaldron/idiomatic.js/tree/master/translations/fr_FR)





## Optimisation


https://auth0.com/blog/four-types-of-leaks-in-your-javascript-code-and-how-to-get-rid-of-them/


You can avoid all this by adding ‘use strict’; at the beginning of your JavaScript file which would switch on a much stricter mode of parsing JavaScript which prevents the unexpected creation of global variables. https://blog.meteor.com/an-interesting-kind-of-javascript-memory-leak-8b47d2e7f156


 Local variables which are captured by a closure are garbage collected once the function they are defined in has finished and all functions defined inside their scope are themselves GCed.

    Always initialize your objects in the same way, so they don’t end up having different shapes.
    Don’t mess with property attributes of array elements, so they can be stored and operated on efficiently.

Currently not optimizable:Functions that contain object literals that contain __proto__, or get or set declarations.
Some of these statements cannot be avoided in production code such as try-finally and try-catch. To use such statements with minimal impact, they must be isolated to a minimal function so that the main code is not affected:

3. Managing argumentsNever use arguments directly without .length or [i]

The arguments object, for example, is an array-like object. It’s possible to call array builtins on it, but such operations won’t be fully optimized the way they could be for a proper array. ES2015 rest parameters can help here. They produce proper arrays that can be used instead of the array-like arguments objects in an elegant way. https://v8.dev/blog/elements-kinds

Avoid polymorphism #

array.push(someValue);



for ... in
Always use Object.keys and iterate over the array with for loop. If you truly need all properties from entire prototype chain, make an isolated helper function: https://github.com/petkaantonov/bluebird/wiki/Optimization-killers


n even shorter way of achieving this is to create a second variable in the pre-loop statement:

var names = ['George','Ringo','Paul','John'];
for(var i=0,j=names.length;i<j;i++){
  doSomeThingWith(names[i]);
}

Keep DOM access to a minimum

https://www.w3.org/wiki/JavaScript_best_practices
