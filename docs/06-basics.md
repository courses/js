### Opérations
**Opérations sur les nombres** : -, +, /, *, % (reste), toFixed(2) pour n'avoir que deux nombres après la virgule

**Opérations sur les strings** :
- + (concaténation). var prenom = 'Paul'; var bonjour = 'Bonjour ' + prenom;
- template strings : var salutation = `Bienvenue et ${bonjour} !`;
- échappement des caractères spéciaux

**Opérations sur booléens** :
- valeurs truthy : true, Objet, Tableau, nombre > 0, Infinity, -Infinity
- valeurs falsy : false, 0, la chaîne vide (""), NaN, null et undefined deviennent toutes false
- opérateur !! : convertir en boolean: var isYoung = 9; !!isYoung returns true.


Bonne pratique le === et le !== : Dynamic


the assignment operator (=) in JavaScript assigns a value to a variable name. And the == and === operators check for equality (the triple === tests for strict equality, meaning both value and type are the same).


The exponentiation operator (**) raises the first operand to the power of the second operand.
 var x = 5;
var z = x ** 2;          // result is 25

[ 1, 2, 3 ] === [ 1, 2, 3 ];    // false
{ a: 42 } === { a: 42 }         // false
(x => x * 2) === (x => x * 2)   // false
var x = [ 1, 2, 3 ];
// assignment is by reference-copy, so
// y references the *same* array as x,
// not another copy of it.
var y = x;

y === x;              // true
y === [ 1, 2, 3 ];    // false
x === [ 1, 2, 3 ];    // false


+=
*=
-=
/=




Les opérateurs && et ||

**Syntaxe de décomposition. Spread operator ...array**
La syntaxe de décomposition permet d'étendre un itérable (par exemple une expression de tableau ou une chaîne de caractères) en lieu et place de plusieurs arguments (pour les appels de fonctions) ou de plusieurs éléments (pour les littéraux de tableaux) ou de paires clés-valeurs (pour les littéraux d'objets).

La syntaxe des paramètres du reste ressemble à la syntaxe de décomposition mais est utilisée afin de destructurer des tableaux et des objets. D'une certaine façon, la syntaxe du reste est l'opposée de la décomposition : la première collecte plusieurs éléments et les condense en un seul élément tandis que la seconde explose les éléments. Pour plus d'informations, voir la page sur les paramètres du reste.


function f(x, y, z) { }
var args = [0, 1, 2];
f(...args);


Ex:concaténer des tableaux:
var arr1 = [0, 1, 2];
var arr2 = [3, 4, 5];
arr1 = [...arr1, ...arr2]; // arr1 vaut [0, 1, 2, 3, 4, 5]


function call: array to list

**affectation par décomposition (destructuring)**

https://dmitripavlutin.com/5-interesting-uses-javascript-destructuring/


L'affectation par décomposition (destructuring en anglais) est une expression JavaScript qui permet d'extraire (unpack en anglais) des données d'un tableau ou d'un objet grâce à une syntaxe dont la forme ressemble à la structure du tableau ou de l'objet.

const toto = ["un", "deux", "trois"];

// sans utiliser la décomposition
const un    = toto[0];
const deux  = toto[1];
const trois = toto[2];

// en utilisant la décomposition
const [un, deux, trois] = toto;


const x = [1, 2, 3, 4, 5]; // On crée un "paquet" de données
const [y, z] = x; // On utilise l'affectation par décomposition
console.log(y); // 1
console.log(z); // 2


const user = { name: 'John Doe', age: 34 };
const { name, age } = user;
const { name: userName, age: userAge } = user; // change names


const user = {
  johnDoe: {
    age: 34,
    email: 'johnDoe@freeCodeCamp.com'
  }
};
const { johnDoe: { age, email }} = user;
const { johnDoe: { age: userAge, email: userEmail }} = user;

const LOCAL_FORECAST = {
  yesterday: { low: 61, high: 75 },
  today: { low: 64, high: 77 },
  tomorrow: { low: 68, high: 80 }
};
const { today: { low: lowToday, high: highToday }} = LOCAL_FORECAST;

// Use Destructuring Assignment to Assign Variables from Arrays
const [a, b] = [1, 2, 3, 4, 5, 6]; // 1, 2
const [a, b,,, c] = [1, 2, 3, 4, 5, 6]; // 1, 2, 5

// Use Destructuring Assignment with the Rest Parameter to Reassign Array Elements
// The rest element only works correctly as the last variable in the list.
const [a, b, ...arr] = [1, 2, 3, 4, 5, 7];
console.log(a, b); // 1, 2
console.log(arr); // [3, 4, 5, 7]


// Use Destructuring Assignment to Pass an Object as a Function's Parameters
const profileUpdate = ({ name, age, nationality, location }) => {
  /* do something with these fields */
}



L'intérêt de l'assignation par décomposition est de pouvoir lire une structure entière en une seule instruction.


// Valeurs par défaut
let a, b;
[a = 5, b = 7] = [1];
console.log(a); // 1
console.log(b); // 7


// Renvoyer plusieurs valeurs
function f() { return [1, 2]; }
function f2() { return [1, 2, 3]; }

let a, b;
[a, b] = f();
const arr = f(); // récupérer la valeur de retour comme un tableau
console.log("A vaut " + a + " B vaut " + b);
const [a, , b] = f(); // ignorer certaines valeurs

//
const [a, ...b] = [1, 2, 3];
console.log(a); // 1
console.log(b); // [2, 3]


// Décomposer un objet
const o = {p: 42, q: true};
const {p: toto, q: truc} = o;
console.log(toto); // 42
console.log(truc); // true

// Affecter de nouveaux noms aux variables et fournir des valeurs par défaut
const {a: aa = 10, b: bb = 5} = {a: 3};
console.log(aa); // 3
console.log(bb); // 5


## Structures de données
### Array

let simpleArray = ['one', 2, 'three', true, false, undefined, null];
console.log(simpleArray.length); // 7

let ourArray = ["a", "b", "c"];
ourArray[1] = "not b anymore";



- push() method adds elements to the end of an array
- unshift() adds elements to the beginning
- pop() removes an element from the end of an array
- shift() removes an element from the beginning.
- splice() allows us to do just that: remove any number of consecutive elements from anywhere in an array.

function mixedNumbers(arr) {
  arr.push('I', 2, 'three' );
  arr.unshift(7, 'VIII', 9 );
  return arr;
}
console.log(mixedNumbers(['IV', 5, 'six']));


let greetings = ['whats up?', 'hello', 'see ya!'];
greetings.pop(); // now equals ['whats up?', 'hello']
greetings.shift(); // now equals ['hello']
let popped = greetings.pop(); // returns 'hello', greetings now equals []


let array = ['I', 'am', 'feeling', 'really', 'happy'];
let newArray = array.splice(3, 2); // newArray equals ['really', 'happy']

const numbers = [10, 11, 12, 12, 15];
const startIndex = 3;
const amountToDelete = 1;
numbers.splice(startIndex, amountToDelete, 13, 14); // the second entry of 12 is removed, and we add 13 and 14 at the same index
console.log(numbers); // returns [ 10, 11, 12, 13, 14, 15 ]


**SLICE (new array)**
let weatherConditions = ['rain', 'snow', 'sleet', 'hail', 'clear'];
let todaysWeather = weatherConditions.slice(1, 3);
// todaysWeather equals ['snow', 'sleet'];
// weatherConditions still equals ['rain', 'snow', 'sleet', 'hail', 'clear']



**SPREAD (copy)**
let thisArray = [true, true, undefined, false, null];
let thatArray = [...thisArray];
// thatArray equals [true, true, undefined, false, null]
// thisArray remains unchanged, and is identical to thatArray

let thisArray = ['sage', 'rosemary', 'parsley', 'thyme'];
let thatArray = ['basil', 'cilantro', ...thisArray, 'coriander'];
// thatArray now equals ['basil', 'cilantro', 'sage', 'rosemary', 'parsley', 'thyme', 'coriander']


**indexOf**
let fruits = ['apples', 'pears', 'oranges', 'peaches', 'pears'];
fruits.indexOf('dates'); // returns -1
fruits.indexOf('oranges'); // returns 2
fruits.indexOf('pears'); // returns 1, the first index at which the element exists

**Loops**
every(), forEach(), map(), etc. for loop.



forEach
map
filter
reduce : accumulation sur une valeur
find
findIndex
from : nodeList to Array



Nom de la méthode 	Description
a.toString() 	Renvoie une chaîne composée des différents éléments auxquels on a appliqué toString(), séparés par des virgules.
a.toLocaleString() 	Renvoie une chaîne composée des différents éléments auxquels on a appliqué toLocaleString(), séparés par des virgules.
a.concat(item1[, item2[, ...[, itemN]]]) 	Renvoie un nouveau tableau auquel on a ajouté les éléments.
a.join(sep) 	Convertit le tableau en une chaîne dont les valeurs sont séparées par le paramètre sep.
a.pop() 	Renvoie le dernier élément du tableau et le retire du tableau.
a.push(item1, ..., itemN) 	Ajoute un ou plusieurs éléments à la fin du tableau.
a.reverse() 	Retourne le tableau.
a.shift() 	Renvoie le premier élément du tableau et le retire du tableau.
a.slice(début[, fin]) 	Renvoie un sous-tableau.
a.sort([cmpfn]) 	Trie le tableau (avec une fonction de comparaison optionnelle).
a.splice(début, delcount[, item1[, ...[, itemN]]]) 	Permet de modifier un tableau en en supprimant une partie et en la remplaçant avec plus d'éléments.
a.unshift(item1[, item2[, ...[, itemN]]]) 	Ajoute des éléments au début du tableau.




    Trailing commas (sometimes called "final commas") can be useful when adding new elements, parameters, or properties to JavaScript code. If you want to add a new property, you can simply add a new line without modifying the previously last line if that line already uses a trailing comma. This makes version-control diffs cleaner and editing code might be less troublesome.

    — Trailing commas at MDN



### Map (ES6)
New kay value data structure (hash map), à la place des objets

- anything as keys
- iterable
- size
- add, delete






## Les structures de contrôle


if
while
do while
switch case

**Boucles**

for
When using a for in loop, usually a good idea to use hasOwnProperty(variable) to make sure the property belongs to the object you want and is not instead an inherited property from the prototype chain:

for (myvariable in object) {
if (object.hasOwnProperty(myvariable)) {
... //statements to be executed
}
}



continue : passe à l'autre iteration
break : sort de la boucle

for (el of arr)

.forEach

.map

for (let value of array) {
  // utiliser des instructions
  // pour manipuler la valeur value
}

for (let propriété in objet) {
  // utiliser des instructions
  // pour manipuler la propriété
}


var permis = (age > 18) ? "oui" : "non";






## ES6 modules


// named function
export default function add(x, y) {
  return x + y;
}

// anonymous function
export default function(x, y) {
  return x + y;
}
Since export default is used to declare a fallback value for a module or file, you can only have one value be a default export in each module or file. Additionally, you cannot use export default with var, let, or const
export const add = (a,b) => a + b;
export const mutliply = (a,b) => a * b;

const add = (x, y) => {
  return x + y;
}

export { add };




import s from './test1.js'
import {add, multiply} from ...
import {add as a, multiply as m} from ...
import * as testImport from ...
